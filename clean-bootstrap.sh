#!/bin/sh
set -x

export LIBVIRT_DEFAULT_URI=qemu:///system
virsh destroy rhyzome-router
virsh destroy rhyzome-apiserver

virsh undefine rhyzome-router
virsh undefine rhyzome-apiserver

virsh vol-delete --pool default rhyzome-router-root.qcow2
virsh vol-delete --pool default rhyzome-apiserver-root.qcow2

virsh net-destroy rhyzome-bootstrap-network

rm -rf pki join-token.txt

true # always exit 0
