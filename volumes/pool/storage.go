// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package pool

import (
	"fmt"
	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
)

// PoolType is an interface for the functionality around a type of StoragePool
type PoolType interface {
	CreateVolume(string, uint64) (*libvirt.StorageVol, error)
	DeleteVolume(string) error
	GetVolumeName(string) string
	ResizeVolume(string, uint64) error
	GetDomainDiskXML(string) libvirtxml.DomainDisk
}

// GetPool retrieves the configured Storage Pool from libvirt
func GetPool(conn *libvirt.Connect) (PoolType, error) {
	pool, err := conn.LookupStoragePoolByName(config.C.DiskStoragePool)
	if err != nil {
		return nil, err
	}

	xmldescription, err := pool.GetXMLDesc(0)
	if err != nil {
		return nil, err
	}

	p := libvirtxml.StoragePool{}
	err = p.Unmarshal(xmldescription)
	if err != nil {
		return nil, err
	}

	switch p.Type {
	case "dir":
		return NewDirPool(pool)
	case "logical":
		return NewLogicalPool(pool)
	}

	return nil, fmt.Errorf("Unknown pool type %s", p.Type)
}
