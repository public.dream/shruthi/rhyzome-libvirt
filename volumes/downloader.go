// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package volumes

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/xi2/xz"
	"libvirt.org/go/libvirt"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
)

func downloadImageFromLegacyStore(imageName string, volume *libvirt.StorageVol, conn *libvirt.Connect) error {
	hashes, err := getHashes(imageName)
	if err != nil {
		return err
	}

	filename := fmt.Sprintf("%s.qcow2", imageName)
	compressed := false
	if _, ok := hashes[filename]; !ok {
		filename = fmt.Sprintf("%s.xz", filename)
		compressed = true
	}

	url := fmt.Sprintf("%s/%s/%s", config.C.ImageHost, imageName, filename)
	return downloadToVolume(url, hashes[filename], compressed, volume, conn)
}

func downloadToVolume(url string, expectedHash string, compressed bool, volume *libvirt.StorageVol, conn *libvirt.Connect) error {
	log.Debugf("downloading %s", url)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Warnf("error fetching image: %s", resp.Status)
		return fmt.Errorf("error fetching base image: %s", resp.Status)
	}

	log.Debug("created hash")
	hash := sha256.New()

	log.Debug("created TeeReader")
	reader := io.TeeReader(resp.Body, hash)

	if compressed {
		log.Debug("image is compressed, decompressing")
		reader, err = xz.NewReader(reader, 0)
		if err != nil {
			return err
		}
	}

	log.Debug("creating libvirt stream")
	stream, err := conn.NewStream(0)
	if err != nil {
		return err
	}

	defer func() {
		// Frees up the stream, if needed
		err = stream.Free()
		if err != nil {
			return
		}
	}()

	log.Debug("creating stream")
	err = volume.Upload(stream, 0, 0, 0)
	if err != nil {
		return err
	}

	log.Debug("uploading to volume")
	err = stream.SendAll(func(s *libvirt.Stream, i int) ([]byte, error) {
		buf := make([]byte, i)
		_, err := reader.Read(buf)
		if err == io.EOF {
			// Go's io interface raise an io.EOF error to indicate the end of the stream,
			// but libvirt indicates EOF with a zero-length response
			return make([]byte, 0), nil
		}
		if err != nil {
			log.Println("Error while reading buffer", err.Error())
			return buf, err
		}
		return buf, nil
	})

	if err != nil {
		return err
	}

	hashstr := hex.EncodeToString(hash.Sum(nil))

	if hashstr != expectedHash {
		return fmt.Errorf("Got bad hash when downloading %s: got %s expected %s", url, hashstr, expectedHash)
	}
	return nil
}

func getHashes(image string) (map[string]string, error) {
	url := fmt.Sprintf("%s/%s/SHA256SUMS", config.C.ImageHost, image)
	log.Debugf("getting URL %s", url)
	resp, err := http.Get(url)
	if err != nil {
		return map[string]string{}, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return map[string]string{}, fmt.Errorf("error fetching base image: %s", resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return map[string]string{}, err
	}

	hashes := make(map[string]string)

	for i, line := range strings.Split(string(body), "\n") {
		if line == "" {
			continue
		}
		lineparts := strings.Split(line, "  ")
		if len(lineparts) != 2 {
			log.Printf("Unable to parse line %d", i)
			continue
		}
		hashes[lineparts[1]] = lineparts[0]
	}
	return hashes, nil
}
