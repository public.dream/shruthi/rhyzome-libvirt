// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package volumes

import (
	"net/url"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/volumes/pool"
)

func Create(id string, size int, image string) error {
	logger := log.WithFields(log.Fields{"volume_id": id})

	logger.Info("creating volume")

	logger.Debug("connecting to libvirt")
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return err
	}
	defer conn.Close()

	storagePool, err := pool.GetPool(conn)
	if err != nil {
		return err
	}

	logger.Debug("creating volume")
	volume, err := storagePool.CreateVolume(id, uint64(size))
	if err != nil {
		return err
	}

	if image != "" {
		logger.Debug("Downloading image")
		start := time.Now()
		if strings.HasPrefix(image, "https://") {
			var imageurl *url.URL
			imageurl, err = url.Parse(image)
			if err == nil {
				err = downloadToVolume(image, imageurl.Fragment, false, volume, conn)
			}
		} else {
			err = downloadImageFromLegacyStore(image, volume, conn)
		}
		if err != nil {
			_ = volume.Delete(0) // If we put a 1 it will overwrite all the data from the disk, see https://libvirt.org/html/libvirt-libvirt-storage.html#virStorageVolDeleteFlags
			return err
		}
		log.Debug("downloaded image in ", time.Since(start))
	}

	log.Debug("volume ready")

	return nil
}

func Delete(id string) error {
	logger := log.WithField("volume_id", id)
	logger.Info("deleting volume")

	logger.Debug("connecting to libvirt")
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return err
	}
	defer conn.Close()

	storagePool, err := pool.GetPool(conn)
	if err != nil {
		return err
	}

	logger.Debug("requesting volume deletion")
	err = storagePool.DeleteVolume(storagePool.GetVolumeName(id))
	if err != nil {
		return err
	}

	logger.Debug("volume deleted")
	return nil
}
