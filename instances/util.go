// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"fmt"
)

func (i Instance) DomainName() string {
	return fmt.Sprintf("rhyzome-%s", i.ID)
}

func idToDomainName(resourceID string) string {
	return fmt.Sprintf("rhyzome-%s", resourceID)
}

func DomainNameToId(domainname string) string {
	return domainname[8:]
}
