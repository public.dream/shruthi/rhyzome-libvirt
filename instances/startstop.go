// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"context"

	log "github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"
)

func Start(ctx context.Context, id string) error {
	logger := log.WithField("instance", id)
	logger.Info("booting instance")

	logger.Debug("connecting to libvirt")
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return err
	}
	defer conn.Close()

	log.Debug("finding domain")
	domain, err := conn.LookupDomainByName(idToDomainName(id))
	if err != nil {
		return err
	}

	log.Debug("creating domain")
	return domain.Create()
}

func Stop(ctx context.Context, id string, graceful bool) error {
	logger := log.WithField("instance", id)
	logger.WithField("graceful", graceful).Info("stopping instance")

	logger.Debug("connecting to libvirt")
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return err
	}
	defer conn.Close()

	log.Debug("finding domain")
	domain, err := conn.LookupDomainByName(idToDomainName(id))
	if err != nil {
		return err
	}

	if graceful {
		logger.Debug("shutting down domain")
		return domain.Shutdown()
	} else {
		logger.Debug("destroying domain")
		return domain.Destroy()
	}
}
