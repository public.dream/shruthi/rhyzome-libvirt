// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"context"

	log "github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"
)

func Delete(ctx context.Context, id string) error {
	logger := log.WithField("instance", id)
	logger.Info("undefining domain")
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return err
	}
	defer conn.Close()

	log.Debug("finding domain")
	domain, err := conn.LookupDomainByName(idToDomainName(id))
	if err != nil {
		return err
	}

	log.Debug("deleting")

	err = domain.Undefine()
	if err != nil {
		logger.Warn("error undefining domain")
		return err
	}
	logger.Info("undefined domain")
	return nil
}
