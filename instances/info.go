// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"
)

func (i Instance) GetDomain(conn *libvirt.Connect) (*libvirt.Domain, error) {
	return conn.LookupDomainByName(i.DomainName())
}

func (i Instance) GetDomainXML(conn *libvirt.Connect) (libvirtxml.Domain, error) {
	domainXML := libvirtxml.Domain{}

	domain, err := i.GetDomain(conn)
	if err != nil {
		return domainXML, err
	}

	xmldoc, err := domain.GetXMLDesc(0)
	if err != nil {
		return domainXML, err
	}

	if err := domainXML.Unmarshal(xmldoc); err != nil {
		return domainXML, err
	}

	return domainXML, nil
}
