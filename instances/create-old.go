// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"context"
	"encoding/base64"
	"fmt"

	log "github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/volumes/pool"
)

func Create(ctx context.Context, id string, volumes []string, userdata []byte, memory int, cores int) error {
	logger := log.WithFields(log.Fields{"instance": id})
	logger.Info("creating instance")

	logger.Debug("connecting to libvirt")
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return err
	}
	defer conn.Close()

	logger.Debug("getting storage pool")
	storagePool, err := pool.GetPool(conn)
	if err != nil {
		return err
	}

	disks := []libvirtxml.DomainDisk{}
	for _, vol := range volumes {
		disks = append(disks, storagePool.GetDomainDiskXML(vol))
	}

	// Create the domain
	domainXML := &libvirtxml.Domain{
		Type:        "kvm",
		Name:        idToDomainName(id),
		Description: base64.StdEncoding.EncodeToString(userdata),
		Memory:      &libvirtxml.DomainMemory{Value: uint(memory), Unit: "MiB"},
		VCPU:        &libvirtxml.DomainVCPU{Value: uint(cores)},
		OS: &libvirtxml.DomainOS{
			Type:        &libvirtxml.DomainOSType{Arch: "x86_64", Type: "hvm"},
			BootDevices: []libvirtxml.DomainBootDevice{{Dev: "hd"}},
		},
		Features: &libvirtxml.DomainFeatureList{
			ACPI:   &libvirtxml.DomainFeature{},
			APIC:   &libvirtxml.DomainFeatureAPIC{},
			VMPort: &libvirtxml.DomainFeatureState{State: "off"},
		},
		CPU: &libvirtxml.DomainCPU{Mode: "host-model"},
		Devices: &libvirtxml.DomainDeviceList{
			Emulator: "/usr/bin/kvm",
			Disks:    disks,
			Interfaces: []libvirtxml.DomainInterface{
				{
					Source: &libvirtxml.DomainInterfaceSource{Bridge: &libvirtxml.DomainInterfaceSourceBridge{Bridge: config.C.BridgeInterface}},
					Model:  &libvirtxml.DomainInterfaceModel{Type: "virtio"},
				},
			},
			Channels: []libvirtxml.DomainChannel{{
				Source: &libvirtxml.DomainChardevSource{UNIX: &libvirtxml.DomainChardevSourceUNIX{Path: "/var/lib/libvirt/qemu/f16x86_64.agent", Mode: "bind"}},
				Target: &libvirtxml.DomainChannelTarget{VirtIO: &libvirtxml.DomainChannelTargetVirtIO{Name: "org.qemu.guest_agent.0"}},
			}},
			Consoles: []libvirtxml.DomainConsole{{Target: &libvirtxml.DomainConsoleTarget{}}},
			Serials:  []libvirtxml.DomainSerial{{}},
		},
		QEMUCommandline: &libvirtxml.DomainQEMUCommandline{
			Args: []libvirtxml.DomainQEMUCommandlineArg{
				{Value: "-smbios"},
				{Value: fmt.Sprintf("type=1,serial=ds=nocloud-net;s=%s/cloud-init/", config.C.MetadataURL)},
			},
		},
	}

	domainXMLString, err := domainXML.Marshal()
	if err != nil {
		return err
	}

	logger.Debug("defining domain from xml")
	domain, err := conn.DomainDefineXML(domainXMLString)
	if err != nil {
		return err
	}

	logger.Debug("booting domain")
	err = domain.Create()
	if err != nil {
		return err
	}

	logger.Debug("domain booted")
	return nil
}
