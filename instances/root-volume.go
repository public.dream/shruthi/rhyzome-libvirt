// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"compress/bzip2"
	"compress/gzip"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
	"hash"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/xi2/xz"
	"libvirt.org/go/libvirt"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/volumes/pool"
)

func (i Instance) createRootVolume(conn *libvirt.Connect) error {
	var reader io.Reader
	var contentLength int64
	start := time.Now()

	parsedImageURL, err := url.Parse(i.Image)
	if err != nil {
		log.Error("error parsing URL")
		return err
	}

	// parse everything after the # in the URL as a query, so chain arguments with &
	fragmentQuery, err := url.ParseQuery(parsedImageURL.Fragment)
	if err != nil && parsedImageURL.Fragment != "" {
		return err
	}

	var expectedHash string
	var downloadedImageHash hash.Hash
	hasHash := false

	// this is fully untested, but the idea is something like:
	// https://example.whatever/img.qcow2#hash=sha256:e8c7c3c983718ebc78d8738f562d55bfa77c4cf6f08241d246861d5ea9eb9cd2
	// sha256 and sha512 supported
	fragmenthash := strings.SplitN(fragmentQuery.Get("hash"), ":", 2)
	if len(fragmenthash) == 2 {
		hasHash = true
		switch fragmenthash[0] {
		case "sha256":
			downloadedImageHash = sha256.New()
		case "sha512":
			downloadedImageHash = sha512.New()
		default:
			return fmt.Errorf("unexpected hash format: %s", fragmenthash[0])
		}
		expectedHash = fragmenthash[1]
	} else if strings.HasPrefix(i.Image, "https://") {
		log.Warnf("downloading image without verifying the hash from %s", i.Image)
	}

	if parsedImageURL.Scheme == "https" || parsedImageURL.Scheme == "http" {
		// check for cached image first
		cacheHash := expectedHash
		if cacheHash == "" {
			cacheHash = fmt.Sprintf("%x", sha256.Sum256([]byte(i.Image)))
		}
		cachedImage := filepath.Join(config.C.ImageCache, cacheHash)

		log.Debug("checking for cache file ", cachedImage, " before downloading from ", i.Image)

		localfile, err := os.Open(cachedImage)
		if err != nil {
			if !os.IsNotExist(err) {
				log.Error("erro reading cache file ", cachedImage, ": ", err)
			}
			log.Debugf("no cached file, downloading %s", i.Image)

			req, err := http.NewRequest("GET", i.Image, nil)
			req.Header.Add("User-Agent", "rhyzome-libvirt") // TODO: put a version string in here too
			if err != nil {
				return err
			}

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			if resp.StatusCode != http.StatusOK {
				log.Warn("error fetching image: ", resp.Status)
				return fmt.Errorf("error fetching base image: %s", resp.Status)
			}

			reader = resp.Body
			contentLength = resp.ContentLength

			if err := os.MkdirAll(config.C.ImageCache, 0755); err != nil {
				log.Error("error ensuring image cache directory ", config.C.ImageCache, ": ", err)
			}

			// localfile, err := os.Create(cachedImage)
			// if err != nil {
			// 	log.Error("error creating cache file: ", err)
			// } else {
			// 	log.Info("caching to ", cachedImage)
			// 	defer localfile.Close()

			// 	reader = io.TeeReader(reader, localfile)
			// }
		} else {
			defer localfile.Close()

			stat, err := localfile.Stat()
			if err != nil {
				log.Warn("error checking metadata on cached image ", cachedImage)
				return err
			}

			reader = localfile
			contentLength = stat.Size()
		}

	} else if parsedImageURL.Scheme == "" {
		log.Debugf("reading local file %s for root volume", parsedImageURL.Path)

		localfile, err := os.Open(parsedImageURL.Path)
		if err != nil {
			return err
		}
		defer localfile.Close()

		stat, err := localfile.Stat()
		if err != nil {
			return err
		}

		reader = localfile
		contentLength = stat.Size()
	} else {
		return fmt.Errorf("unsupported image URL scheme %s", parsedImageURL.Scheme)
	}

	if expectedHash != "" {
		reader = io.TeeReader(reader, downloadedImageHash)
	}

	// check for known compression suffixes and decompress
	// can also be enabled by adding to the URL fragment:
	// https://example.whatever/img.qcow2#compression=xz
	fragmentCompression := fragmentQuery.Get("compression")
	if strings.HasSuffix(parsedImageURL.Path, ".xz") || fragmentCompression == "xz" {
		log.Debug("image is xz compressed, decompressing")
		reader, err = xz.NewReader(reader, 0)
		if err != nil {
			return err
		}
	} else if strings.HasSuffix(parsedImageURL.Path, ".gz") || fragmentCompression == "gzip" {
		log.Debug("image is gz compressed, decompressing")
		reader, err = gzip.NewReader(reader)
		if err != nil {
			return err
		}
	} else if strings.HasSuffix(parsedImageURL.Path, ".bz2") || fragmentCompression == "bzip2" {
		log.Debug("image is bzip2 compressed, decompressing")
		reader = bzip2.NewReader(reader)
	}

	log.Debug("creating libvirt volume")
	storagePool, err := pool.GetPool(conn)
	if err != nil {
		return err
	}

	volume, err := storagePool.CreateVolume(i.RootVolumeName(), uint64(i.DiskSizeGB))
	if err != nil {
		return err
	}

	stream, err := conn.NewStream(0)
	if err != nil {
		return err
	}
	defer func() {
		// Frees up the stream, if needed
		err := stream.Free()
		if err != nil {
			log.Error("error freeing volume stream: ", err.Error())
		}
	}()

	err = volume.Upload(stream, 0, 0, 0)
	if err != nil {
		return err
	}

	log.Debug("uploading to volume")
	totalRead := 0
	nextLog := time.Now()
	err = stream.SendAll(func(s *libvirt.Stream, i int) ([]byte, error) {
		out := []byte{}

		for len(out) < i {
			buf := make([]byte, i-len(out))
			// fill the buffer of i bytes from the reader
			read, err := reader.Read(buf)
			if read > 0 {
				out = append(out, buf[:read]...)
				totalRead += read
			}
			if (contentLength > 0 && time.Until(nextLog) <= 0) || (int64(totalRead) == contentLength && read > 0) {
				log.Debug("transfer progress: ", totalRead, "/", contentLength, " (", int((float64(totalRead)/float64(contentLength))*100), "%) ", time.Since(start))
				nextLog = time.Now().Add(time.Second)
			}
			if err == io.EOF {
				if read > 0 {
					// partial read before hitting EOF, return buffer
					return out, nil
				}
				// Go's io interface raise an io.EOF error to indicate the end of the stream,
				// but libvirt indicates EOF with a zero-length response
				return make([]byte, 0), nil
			}
			if err != nil {
				log.Println("Error while reading buffer", err.Error())
				return out, err
			}
		}
		return out, nil
	})
	if err != nil {
		return err
	}

	if hasHash {
		hashstr := fmt.Sprintf("%x", downloadedImageHash.Sum(nil))

		if hashstr != expectedHash {
			return fmt.Errorf("Got bad hash when downloading %s: got %s expected %s", i.Image, hashstr, expectedHash)
		}
		log.Debug("hash of downloaded image matched expected")
	}

	// qcow2 quirk: because we download a qcow2 file instead of the raw file, the server-provided metadata about the virtual
	// volume is left in tact, which sets the virtual disk size to the size the server wants it to be, ignoring our requested
	// size. To work around this, we resize the virtual disk size before first boot.
	if err := storagePool.ResizeVolume(i.RootVolumeName(), uint64(i.DiskSizeGB)); err != nil {
		log.Error("error resizing volume after download: ", err)
		return err
	}

	return nil
}
