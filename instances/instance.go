// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"fmt"
)

// SpecialCase are different instance configurations that can be requested
type SpecialCase string

var (
	// SpecialCaseRouter means the instance gets a WAN port, maybe some other stuff
	SpecialCaseRouter SpecialCase = "router"

	// SpecialCaseCloudInitNoCloud passes the configured metadata server to the instance via qemu's -smbios flag
	SpecialCaseCloudInitNoCloud SpecialCase = "cloud-init-nocloud"

	// SpecialCaseControlNetwork causes an instance to be added to the control network
	SpecialCaseControlNetwork SpecialCase = "control-network"
)

// Instance represents an virtual machine. But it's 2022 so we call them "instances"
type Instance struct {
	ID           string        // The instance ID
	Image        string        // Image is the URL of the image to write to the root disk
	DiskSizeGB   int           // DiskSizeGB is the size (in gigabytes) to allocate for the root disk
	MemoryMB     int           // MemoryMB is the amount of memory (RAM) to allocate to the instance
	Cores        int           // Cores is the number of CPU cores to allocate to the instance
	Volumes      []string      // Volumes is a list of volume IDs to attach to this instance
	Networks     []string      // Networks is a list of network IDs to attach to this instance - currently unused
	SpecialCases []SpecialCase // SpecialCase is a list of special cases to apply. See SpecialCase type for options and details. Do not allow to be written by the API.

	Smbios map[int]map[string]string // Smbios is the smbios data to pass to qemu. int key is type, string key is smbios field, value is value. See https://gist.github.com/smoser/290f74c256c89cb3f3bd434a27b9f64c for mappings
}

// RootVolumeName returns the name of the instance's root volume
func (i Instance) RootVolumeName() string {
	return fmt.Sprintf("rhyzome-%s-root", i.ID)
}

func (i Instance) hasSpecialCase(specialcase SpecialCase) bool {
	for _, s := range i.SpecialCases {
		if s == specialcase {
			return true
		}
	}
	return false
}

func (i *Instance) addSpecialCase(specialcase SpecialCase) {
	if i.hasSpecialCase(specialcase) {
		return
	}
	i.SpecialCases = append(i.SpecialCases, specialcase)
}
