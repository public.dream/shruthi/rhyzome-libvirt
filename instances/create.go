// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/volumes/pool"
)

const (
	// BootstrapNetworkName is the name of the ephemeral libvirt network used for bootstrapping the cluster
	BootstrapNetworkName = "rhyzome-bootstrap-network"
)

// Create the instance in libvirt
func (i Instance) Create(conn *libvirt.Connect) error {
	logger := log.WithFields(log.Fields{"instance": i.ID})
	logger.Info("creating instance")

	logger.Debug("getting storage pool")
	storagePool, err := pool.GetPool(conn)
	if err != nil {
		return err
	}

	logger.Debug("creating root volume")
	err = i.createRootVolume(conn)
	if err != nil {
		return err
	}

	logger.Debug("preparing domain XML")
	disks := []libvirtxml.DomainDisk{storagePool.GetDomainDiskXML(i.RootVolumeName())}
	for _, vol := range i.Volumes {
		disks = append(disks, storagePool.GetDomainDiskXML(vol))
	}

	if i.Smbios == nil {
		i.Smbios = make(map[int]map[string]string)
	}

	if _, ok := i.Smbios[3]; !ok {
		i.Smbios[3] = make(map[string]string)
	}
	if _, ok := i.Smbios[3]["manufacturer"]; !ok {
		i.Smbios[3]["manufacturer"] = "rhyzome.dev"
	}

	if i.hasSpecialCase(SpecialCaseCloudInitNoCloud) {
		if _, ok := i.Smbios[1]; !ok {
			i.Smbios[1] = make(map[string]string)
		}
		i.Smbios[1]["serial"] = fmt.Sprintf("ds-nocloud-net;s=%s/cloud-init/", config.C.MetadataURL)
	}

	smbios := []libvirtxml.DomainQEMUCommandlineArg{}
	for smbiosType, values := range i.Smbios {
		arg := libvirtxml.DomainQEMUCommandlineArg{
			Value: fmt.Sprintf("type=%d", smbiosType),
		}
		for key, value := range values {
			arg.Value = fmt.Sprintf("%s,%s=%s", arg.Value, key, value)
		}
		smbios = append(smbios, libvirtxml.DomainQEMUCommandlineArg{Value: "-smbios"}, arg)
	}

	interfaces := []libvirtxml.DomainInterface{}
	if i.hasSpecialCase(SpecialCaseControlNetwork) {
		interfaces = append(interfaces,
			// the WAN interface for the router
			libvirtxml.DomainInterface{
				Source: &libvirtxml.DomainInterfaceSource{Bridge: &libvirtxml.DomainInterfaceSourceBridge{Bridge: config.C.BridgeInterface}},
				Model:  &libvirtxml.DomainInterfaceModel{Type: "virtio"},
			},
		)
	}
	if i.hasSpecialCase(SpecialCaseRouter) {
		interfaces = append(interfaces,
			libvirtxml.DomainInterface{
				Source: &libvirtxml.DomainInterfaceSource{
					Network: &libvirtxml.DomainInterfaceSourceNetwork{
						Network: BootstrapNetworkName,
					},
				},
				Model: &libvirtxml.DomainInterfaceModel{Type: "virtio"},
			},
		)
	}

	if len(i.Networks) > 0 {
		log.Warn("adding instances to networks unsupported! Please fix this")
	}

	// for _, networkID := range i.Networks {
	// 	// TODO: fix this to work with devan's networking code
	// 	interfaces = append(interfaces, libvirtxml.DomainInterface{
	// 		Source: &libvirtxml.DomainInterfaceSource{
	// 			Bridge: &libvirtxml.DomainInterfaceSourceBridge{Bridge: config.C.BridgeInterface},
	// 		},
	// 		PortOptions: &libvirtxml.DomainInterfacePortOptions{Isolated: "true"},
	// 		VLan: &libvirtxml.DomainInterfaceVLan{Tags: []libvirtxml.DomainInterfaceVLanTag{
	// 			libvirtxml.DomainInterfaceVLanTag{ID: uint(vlanID)}},
	// 		},
	// 	})
	// }

	// Create the domain
	domainXML := &libvirtxml.Domain{
		Type:   "kvm",
		Name:   i.DomainName(),
		Memory: &libvirtxml.DomainMemory{Value: uint(i.MemoryMB), Unit: "MiB"},
		VCPU:   &libvirtxml.DomainVCPU{Value: uint(i.Cores)},
		OS: &libvirtxml.DomainOS{
			Type:        &libvirtxml.DomainOSType{Arch: "x86_64", Type: "hvm"},
			BootDevices: []libvirtxml.DomainBootDevice{{Dev: "hd"}},
		},
		Features: &libvirtxml.DomainFeatureList{
			ACPI:   &libvirtxml.DomainFeature{},
			APIC:   &libvirtxml.DomainFeatureAPIC{},
			VMPort: &libvirtxml.DomainFeatureState{State: "off"},
		},
		CPU: &libvirtxml.DomainCPU{Mode: "host-model"},
		Devices: &libvirtxml.DomainDeviceList{
			Emulator: "/usr/bin/kvm",
			Disks:    disks,
			Channels: []libvirtxml.DomainChannel{{
				Source: &libvirtxml.DomainChardevSource{
					UNIX: &libvirtxml.DomainChardevSourceUNIX{Path: "/var/lib/libvirt/qemu/f16x86_64.agent", Mode: "bind"},
				},
				Target: &libvirtxml.DomainChannelTarget{
					VirtIO: &libvirtxml.DomainChannelTargetVirtIO{Name: "org.qemu.guest_agent.0"},
				},
			}},
			Consoles:   []libvirtxml.DomainConsole{{Target: &libvirtxml.DomainConsoleTarget{}}},
			Serials:    []libvirtxml.DomainSerial{{}},
			Interfaces: interfaces,
		},
		QEMUCommandline: &libvirtxml.DomainQEMUCommandline{Args: smbios},
	}

	domainXMLString, err := domainXML.Marshal()
	if err != nil {
		return err
	}

	logger.Debug("defining domain from xml")
	domain, err := conn.DomainDefineXML(domainXMLString)
	if err != nil {
		return err
	}

	logger.Debug("booting domain")
	err = domain.Create()
	if err != nil {
		return err
	}

	logger.Debug("domain booted")
	return nil
}
