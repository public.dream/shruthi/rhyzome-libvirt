// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"io"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

const maxBackoff = 60 // max 60 second backoff

var (
	conn              *grpc.ClientConn
	LibvirtClient     rhyzome_protos.RhyzomeLibvirtClient
	eventStreamClient rhyzome_protos.RhyzomeLibvirt_EventStreamClient
	shutdown          = false
	backoff           = 1
)

func Connect() {
	for {
		if shutdown {
			log.Debug("shutdown = true, not re-connecting")
			return
		}
		delay := time.Duration(backoff) * time.Second
		log.Debug("connecting to grpc server in ", delay)
		time.Sleep(time.Duration(backoff) * time.Second)

		if err := connect(); err != nil {
			log.Error("error connecting to grpc server: ", err)
			increaseBackoff()
		}
	}
}

func ConnectOneShot() error {
	return connect()
}

func connect() error {
	// parts := strings.Split(config.C.APIServer, ":")
	// host := parts[0]

	certificate, err := config.C.PKI.GetX509KeyPair()
	if err != nil {
		log.Error("error loading x509 pair: ", err)
		return err
	}

	certPool := x509.NewCertPool()
	bs, err := config.C.PKI.GetCA()
	if err != nil {
		log.Error("error reading CA file: ", err)
		return err
	}

	ok := certPool.AppendCertsFromPEM(bs)
	if !ok {
		err = errors.New("error appending root CA to pool")
		log.Error(err)
		return err
	}

	tlsConfig := credentials.NewTLS(&tls.Config{
		// ServerName:   host,
		Certificates: []tls.Certificate{certificate},
		RootCAs:      certPool,
	})

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	log.Info("establishing grpc connection to ", config.C.APIServer)
	conn, err = grpc.DialContext(ctx, config.C.APIServer, grpc.WithTransportCredentials(tlsConfig))
	if err != nil {
		log.Error("error dialing API server: ", err)
		return err
	}

	LibvirtClient = rhyzome_protos.NewRhyzomeLibvirtClient(conn)

	log.Info("connecting to event stream")
	eventStreamClient, err = LibvirtClient.EventStream(context.Background())
	if err != nil {
		log.Error("error getting event stream from libvirt client: ", err)
		return err
	}

	log.Info("connected, processing incoming events")
	processEvents()

	return nil
}

func Shutdown() error {
	shutdown = true
	if conn == nil {
		return nil
	}
	log.Info("shutting down grpc client")
	return conn.Close()
}

func processEvents() {
	for {
		if shutdown {
			return
		}
		event, err := eventStreamClient.Recv()
		if err != nil {
			if err == io.EOF {
				log.Info("Got EOF from GRPC server")
			} else {
				log.Error("error receiving event from grpc stream: ", err)
			}
			increaseBackoff()
			return
		}

		go processEvent(eventStreamClient.Context(), event)
		backoff = 1
	}
}

func increaseBackoff() {
	backoff = backoff * 2
	if backoff > maxBackoff {
		backoff = maxBackoff
	}
}
