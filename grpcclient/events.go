// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"

	log "github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/instances"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/volumes"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

func processEvent(ctx context.Context, event *rhyzome_protos.LibvirtEvent) {
	var err error
	switch {
	case event.CreateInstance != nil:
		err = createInstance(ctx, event)
	case event.StartInstance != nil:
		err = startInstance(ctx, event)
	case event.StopInstance != nil:
		err = stopInstance(ctx, event)
	case event.DeleteInstance != nil:
		err = deleteInstance(ctx, event)
	}
	if err != nil {
		log.Error("error handling event: ", err)
		err = eventStreamClient.Send(&rhyzome_protos.LibvirtEventUpdate{
			EventId: event.EventId,
			Type:    rhyzome_protos.LibvirtEventUpdate_FAILED,
			Message: err.Error(),
		})
		if err != nil {
			log.Error("error submitting event update: ", err)
		}
	} else {
		err = eventStreamClient.Send(&rhyzome_protos.LibvirtEventUpdate{
			EventId: event.EventId,
			Type:    rhyzome_protos.LibvirtEventUpdate_SUCCESS,
		})
		if err != nil {
			log.Error("error submitting event update: ", err)
		}
	}
	backoff = 1
}

func startInstance(ctx context.Context, event *rhyzome_protos.LibvirtEvent) error {
	err := instances.Start(ctx, event.StartInstance.ResourceId)
	if err != nil {
		return err
	}
	return nil
}

func stopInstance(ctx context.Context, event *rhyzome_protos.LibvirtEvent) error {
	err := instances.Stop(ctx, event.StopInstance.ResourceId, true)
	if err != nil {
		return err
	}
	return nil
}

func createInstance(ctx context.Context, event *rhyzome_protos.LibvirtEvent) error {
	e := event.CreateInstance
	log.Debugf("create instance event: %#v", event.CreateInstance)

	instance := instances.Instance{
		ID:         e.ResourceId,
		Image:      e.RootVolume.SourceImage,
		DiskSizeGB: int(e.RootVolume.Size),
		MemoryMB:   int(e.Memory * 1024),
		Cores:      int(e.Cores),
	}

	c, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return err
	}
	defer c.Close()

	err = instance.Create(c)
	if err != nil {
		return nil
	}

	return nil
}

func deleteInstance(ctx context.Context, event *rhyzome_protos.LibvirtEvent) error {
	instanceID := event.DeleteInstance.ResourceId

	err := instances.Stop(ctx, instanceID, true)
	if err != nil {
		if libvirtErr, ok := err.(libvirt.Error); ok && libvirtErr.Is(libvirt.ERR_NO_DOMAIN) {
			log.Info("domain is already deleted")
		} else {
			return err
		}
	}

	err = instances.Delete(ctx, instanceID)
	if err != nil {
		if libvirtErr, ok := err.(libvirt.Error); ok && libvirtErr.Is(libvirt.ERR_NO_DOMAIN) {
			log.Info("domain is already deleted")
		} else {
			return err
		}
	}

	err = volumes.Delete(event.DeleteInstance.RootVolumeId)
	if err != nil {
		if libvirtErr, ok := err.(libvirt.Error); ok && libvirtErr.Is(libvirt.ERR_NO_STORAGE_VOL) {
			log.Info("volume is already deleted")
		} else {
			return err
		}
	}

	return nil
}
