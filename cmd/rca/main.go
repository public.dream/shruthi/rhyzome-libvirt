// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import "git.callpipe.com/entanglement.garden/rhyzome-libvirt/cmd/rca/cmd"

func main() {
	cmd.Execute()
}
