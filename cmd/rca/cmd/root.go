// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/cmd/rca/cmd/bootstrap"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/cmd/rca/cmd/buildvm"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/cmd/rca/cmd/join"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
)

var rootCmd = &cobra.Command{
	Use:   "rca",
	Short: "Rhyzome Cluster Admin utility cli, operated from a shell on a hypervisor",
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(config.Load)
	rootCmd.AddCommand(bootstrap.BootstrapCmd)
	rootCmd.AddCommand(buildvm.BuildVMCmd)
	rootCmd.AddCommand(join.JoinCmd)
}
