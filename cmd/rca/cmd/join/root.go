// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package join

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
)

var (
	rhyzomePath = "/etc/rhyzome"
	stepPath    = filepath.Join(rhyzomePath, "step")
	caPath      = filepath.Join(rhyzomePath, "ca.pem")

	baseURL       url.URL
	token         string
	caFingerprint string
	JoinCmd       = &cobra.Command{
		Use:   "join rhyzome://...",
		Short: "accepts a rhyzome joining URL, joins the local machine to the cluster",
		PreRun: func(_ *cobra.Command, args []string) {
			if len(args) != 1 {
				log.Fatal("exactly one argument expected")
			}

			u, err := url.Parse(args[0])
			if err != nil {
				log.Fatal("error prasing URL from arguments: ", err)
			}

			baseURL = url.URL{
				Scheme: "https",
				Host:   u.Host,
				Path:   u.Path,
			}

			if !strings.HasSuffix(baseURL.Path, "/") {
				baseURL.Path = baseURL.Path + "/"
			}

			token = u.Query().Get("token")
			if token == "" {
				log.Fatal("error: token is empty")
			}

			caFingerprint = u.Query().Get("ca")
		},
		Run: func(_ *cobra.Command, _ []string) {
			provisioningInfo, err := GetProvisioningInfo(baseURL, caFingerprint)
			if err != nil {
				log.Fatal("error getting provisionering info: ", err)
			}
			log.Debugf("got provisioning info: %+v", provisioningInfo)

			if err := RunStepCommand("ca", "bootstrap", "--force", "--ca-url", provisioningInfo.Endpoints.Step, "--fingerprint", caFingerprint); err != nil {
				log.Fatal("error initializing step locally: ", err)
			}

			subject, err := GetJWTSubject(token)
			if err != nil {
				log.Fatal("error parsing join token: ", err)
			}

			certPath := filepath.Join(stepPath, fmt.Sprintf("%s.crt", subject))
			keyPath := filepath.Join(stepPath, fmt.Sprintf("%s.key", subject))

			if err := RunStepCommand("ca", "certificate", "--force", "--token", token, subject, certPath, keyPath); err != nil {
				log.Fatal("error getting certificate: ", err)
			}
			log.Info("local certificates configured in ", filepath.Join(stepPath, fmt.Sprintf("%s.crt", subject)))

			newconfig := config.C
			newconfig.APIServer = provisioningInfo.Endpoints.Grpc
			newconfig.PKI.Cert = certPath
			newconfig.PKI.Key = keyPath
			newconfig.PKI.CA = caPath

			f, err := os.Create(config.ConfigFiles[0])
			if err != nil {
				log.Fatal("error creating ", config.ConfigFiles[0], ": ", err)
			}
			defer f.Close()

			err = json.NewEncoder(f).Encode(&newconfig)
			if err != nil {
				log.Fatal("error encoding new config: ", err)
			}

			log.Info("Wrote config to ", config.ConfigFiles[0])
		},
	}
)
