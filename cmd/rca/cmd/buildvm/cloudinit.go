// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package buildvm

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

type cloudConfigStruct struct {
	Packages          []string              `yaml:"packages"`
	SSHAuthorizedKeys []string              `yaml:"ssh_authorized_keys"`
	PhoneHome         phoneHomeConfigStruct `yaml:"phone_home"`
}

type phoneHomeConfigStruct struct {
	URL string `yaml:"url"`
}

type cloudConfigFile struct {
	Content     string `yaml:"content"`
	Path        string `yaml:"path"`
	Permissions string `yaml:"permissions"`
}

var (
	cloudConfig = cloudConfigStruct{
		Packages:          []string{},
		SSHAuthorizedKeys: []string{},
	}
)

func prepareCloudInit(phoneHomeIP string) {
	cloudConfig.PhoneHome.URL = fmt.Sprintf("http://%s:8080/setup-complete", phoneHomeIP)
	cloudConfig.Packages = packages

	// SSH keys
	for _, source := range sshkeys {
		u, err := url.Parse(source)
		if err != nil {
			log.Fatal("error parsing SSH key url ", source, " :", err)
		}

		switch u.Scheme {
		case "": // local disk
			if strings.HasPrefix(u.Path, "~") {
				home, _ := os.UserHomeDir()
				u.Path = strings.Replace(u.Path, "~", home, 1)
			}
			keys, err := ioutil.ReadFile(u.Path)
			if err != nil {
				log.Fatal("error reading SSH key file ", u.Path, ": ", err)
			}
			for _, key := range strings.Split(string(keys), "\n") {
				cloudConfig.SSHAuthorizedKeys = append(cloudConfig.SSHAuthorizedKeys, strings.TrimSpace(key))
			}
		case "https":
			resp, err := http.Get(source)
			if err != nil {
				log.Fatal("error fetching SSH key from ", source, ": ", err)
			}
			defer resp.Body.Close()

			keys, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Fatal("error reading SSH key file ", u.Path, ": ", err)
			}
			for _, key := range strings.Split(string(keys), "\n") {
				cloudConfig.SSHAuthorizedKeys = append(cloudConfig.SSHAuthorizedKeys, strings.TrimSpace(key))
			}
		}
	}
}
