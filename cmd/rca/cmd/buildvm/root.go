// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package buildvm

import (
	"errors"
	"fmt"
	"net"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
	"libvirt.org/go/libvirt"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/instances"
)

var (
	image    string
	memoryMB int
	diskGB   int
	pool     string
	iface    string
	name     string
	sshkeys  []string
	packages []string
)

// BuildVMCmd is the subcommand to build a a one-off VM outside the system
var BuildVMCmd = &cobra.Command{
	Use:   "build-vm [--image url] [--memory mb] [--disk gb] [--pool storage-pool] [--interface iface] [--package packages] [--ssh-key sshkey] vmname",
	Short: "create a vm outside the rhyzome system. do not use.",
	Long:  "build and boot a vm outside of the rest of the system. This is a convenience function because the rest of the system isn't quite there yet and i want to boot VMs now!",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return errors.New("incorrect number of arguments")
		}
		name = args[0]

		config.C.BridgeInterface = iface
		config.C.DiskStoragePool = pool

		return nil
	},
	Run: func(_ *cobra.Command, _ []string) {
		conn, err := libvirt.NewConnect("qemu:///system")
		if err != nil {
			log.Fatal("unable to connect to libvirt: ", err)
		}
		defer conn.Close()

		ifaces, err := net.Interfaces()
		if err != nil {
			log.Fatal("error finding interface: ", err)
		}

		bindIP := ""
		for _, i := range ifaces {
			if i.Name == iface {
				addrs, err := i.Addrs()
				if err != nil {
					log.Fatal("error getting IPs for interface ", i.Name, ": ", err)
				}
				bindIP = strings.Split(addrs[0].String(), "/")[0]
			}
		}
		if bindIP == "" {
			log.Fatal("could not find IP for interface ", iface)
		}

		prepareCloudInit(bindIP)

		// start metadata server
		go httpListener(bindIP)

		cloudconfig, err := yaml.Marshal(cloudConfig)
		if err != nil {
			log.Fatal("error preparing cloud-config: ", err)
		}

		userdata = append([]byte("#cloud-config\n"), cloudconfig...)

		// create the vm
		vm := instances.Instance{
			ID:         name,
			Image:      image,
			DiskSizeGB: diskGB,
			MemoryMB:   memoryMB,
			Cores:      1,
			Smbios: map[int]map[string]string{
				1: map[string]string{
					"serial": fmt.Sprintf("ds=nocloud-net;s=http://%s:8080/", bindIP),
				},
			},
			SpecialCases: []instances.SpecialCase{
				instances.SpecialCaseControlNetwork,
			},
		}
		err = vm.Create(conn)
		if err != nil {
			log.Fatal("error creating vm: ", err)
		}

		select {}
	},
}

func init() {
	BuildVMCmd.Flags().StringVar(&image, "image", "https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2", "the URL or path to local file to use for the bootstrap VM")
	BuildVMCmd.Flags().IntVar(&memoryMB, "memory", 1024, "the amount of memory, in MB, to allocate the new VM")
	BuildVMCmd.Flags().IntVar(&diskGB, "disk", 20, "the amount of storage, in GB, to allocate the new VM")
	BuildVMCmd.Flags().StringVar(&pool, "pool", "default", "the storage pool to create the root disk on")
	BuildVMCmd.Flags().StringVar(&iface, "interface", "br0", "the bridge interface")
	BuildVMCmd.Flags().StringArrayVar(&sshkeys, "ssh-key", []string{"~/.ssh/id_ed25519.pub"}, "ssh keys to provision on the new VM. Files and https URLs supported")
	BuildVMCmd.Flags().StringArrayVar(&packages, "package", []string{}, "packages to install in the vm")
}
