// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package buildvm

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var (
	metadataPort = 8080
	userdata     []byte
)

func httpListener(bindIP string) {
	http.HandleFunc("/user-data", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/yaml")
		if _, err := w.Write(userdata); err != nil {
			log.Error("error writing http response: ", err)
		}
		log.Info("cloudconfig started")
	})

	http.HandleFunc("/meta-data", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/yaml")
		type cloudInitMetaDataResponse struct {
		}
		err := yaml.NewEncoder(w).Encode(map[string]string{
			"local-hostname": name,
			"instance-id":    name,
			"cloud-name":     "rhyzome.dev",
		})
		if err != nil {
			log.Error("Error encoding response", err)
			http.Error(w, err.Error(), 500)
		}
	})

	http.HandleFunc("/vendor-data", func(w http.ResponseWriter, r *http.Request) {
		if _, err := w.Write([]byte{}); err != nil {
			log.Error("error writing http response: ", err)
		}
	})

	http.HandleFunc("/setup-complete", func(w http.ResponseWriter, r *http.Request) {
		hostKeyEd25519 := r.PostFormValue("pub_key_ed25519")
		remoteIP := strings.Split(r.RemoteAddr, ":")[0]

		if _, err := w.Write([]byte{}); err != nil {
			log.Error("error writing http response: ", err)
		}

		go func() {
			time.Sleep(time.Second)
			log.Info("bootstrapping complete, your cluster is now running on ", remoteIP, " with SSH public key ", hostKeyEd25519)
			os.Exit(0)
		}()
	})

	addr := fmt.Sprintf("%s:%d", bindIP, metadataPort)
	log.Debug("starting metadata http server on ", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatal("error starting http listener on ", addr, ":", err)
	}
}
