// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
	"libvirt.org/go/libvirt"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/instances"
)

const (
	defaultDebianURL = "https://cloud.debian.org/images/cloud/bookworm/daily/latest/debian-12-generic-amd64-daily.qcow2"
	openwrtURL       = "https://git.callpipe.com/entanglement.garden/vm-images/openwrt/-/jobs/16753/artifacts/raw/openwrt.qcow2#hash=sha256:e2cc36fc21c6d91bc6e6809878d29b3cd9830005d00c6acc5594a3865695a088"
)

var (
	bootstrapImage    string
	joinTokenFilename string
	dns               string
	registry          string
)

// BootstrapCmd is the subcommand to bring up a new cluster
var BootstrapCmd = &cobra.Command{
	Use:   "bootstrap [--image url] [--join-token filename] [--dns domain] [--registry docker.io]",
	Short: "bootstrap a new PKI and cluster",
	Long: `Bootstrap a new cluster

Uses configured libvirt connection to create a virtual network and two virtual machines:

* One runs openwrt and acts as a gateway, offering VLANs, wireguard, DHCP, and some other things.
* The other runs Debian (testing), with all core services, including Ory, the core rhyzome
	services, the online CA software (step).

When the VMs start, tokens that can be exchanged for signed certificates are given out to various
components, including rhyzome-libvirt, which is expected to be running unconfigured on localhost.
it will configure itself when it receives the token.
`,
	Run: func(cmd *cobra.Command, args []string) {
		readLocalSSHKey()

		// create bootstrap network
		conn, err := libvirt.NewConnect("qemu:///system")
		if err != nil {
			log.Fatal("unable to connect to libvirt: ", err)
		}
		defer conn.Close()

		err = ensurePool(conn)
		if err != nil {
			log.Fatal("error ensuring volume: ", err)
		}

		if err := createLibvirtNetwork(conn); err != nil {
			log.Fatal("error creating network: ", err)
		}

		bindIP, err := getBridgeIP()
		if err != nil {
			log.Fatal("error getting bridge ip: ", err)
		}

		addr := fmt.Sprintf("%s:%d", bindIP, metadataPort)

		// start metadata server
		go httpListener(addr)

		cloudConfig.RunCmd = [][]string{[]string{"/opt/bootstrap.sh", "http://192.168.1.1:8080", fmt.Sprintf("http://%s", addr), dns}}

		cloudConfig.WriteFile(cloudConfigFile{
			Path:        "/etc/containers/registries.conf.d/registry.conf",
			Content:     fmt.Sprintf("unqualified-search-registries = [\"%s\"]\n", registry),
			Permissions: "0644",
		})

		apiserverMetadata, err := yaml.Marshal(cloudConfig)
		if err != nil {
			log.Fatal("error preparing apiserver user data: ", err)
		}

		// create bootstrap apiserver
		apiserver := instances.Instance{
			ID:         "apiserver",
			Image:      bootstrapImage,
			DiskSizeGB: 10,
			MemoryMB:   512,
			Cores:      1,
			Smbios: map[int]map[string]string{
				1: map[string]string{
					"serial": "ds=nocloud-net;s=http://192.168.1.1:8080/cloud-init/",
				},
			},
			SpecialCases: []instances.SpecialCase{
				instances.SpecialCaseControlNetwork,
			},
		}
		err = apiserver.Create(conn)
		if err != nil {
			log.Fatal("error creating apiserver vm: ", err)
		}

		apiserverXML, err := apiserver.GetDomainXML(conn)
		if err != nil {
			log.Fatal("error getting apiserver domain xml: ", err)
		}

		routerBootstrapData, err = json.Marshal(map[string]string{
			"bootstrap_mac":      apiserverXML.Devices.Interfaces[0].MAC.Address,
			"bootstrap_userdata": string(append([]byte("#cloud-config\n"), apiserverMetadata...)),
		})
		if err != nil {
			log.Fatal("error preparing router user data: ", err)
		}

		router := instances.Instance{
			ID:         "router",
			Image:      openwrtURL,
			DiskSizeGB: 2,
			MemoryMB:   512,
			Cores:      1,
			SpecialCases: []instances.SpecialCase{
				instances.SpecialCaseControlNetwork,
				instances.SpecialCaseRouter,
			},
			Smbios: map[int]map[string]string{
				1: map[string]string{
					"serial": fmt.Sprintf("ds=nocloud-net;s=http://%s/", addr),
				},
			},
		}
		err = router.Create(conn)
		if err != nil {
			log.Fatal("error creating router: ", err)
		}

		select {}
	},
}

func init() {
	BootstrapCmd.Flags().StringVar(&bootstrapImage, "image", defaultDebianURL, "the URL or path to local file to use for the bootstrap VM")
	BootstrapCmd.Flags().StringVar(&joinTokenFilename, "join-token", "", "the file to write the join token to")
	BootstrapCmd.Flags().StringVar(&dns, "dns", "rhyzome.localtest.me", "the root domain to use for all services. Can be a subdomain. a random one that will not work externally will be generated if not specified.")
	BootstrapCmd.Flags().StringVar(&registry, "registry", "docker.io", "the registry or registry proxy to use for all images")
}
