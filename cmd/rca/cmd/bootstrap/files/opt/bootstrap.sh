#!/bin/bash
# Copyright 2021 Entanglement Garden Developers
# SPDX-License-Identifier: AGPL-3.0-only
set -exuo pipefail

function generate_random() {
    python3 -c "import secrets; print(secrets.token_urlsafe($1))"
}

function generate_random_hex() {
    python3 -c "import secrets; print(secrets.token_hex($1))"
}

domain=${3}
for template in /etc/haproxy/haproxy.cfg /etc/ory/kratos/kratos.yml /etc/ory/hydra.yml /etc/rhyzome/web.json; do
    sed -i "s#DOMAIN#${domain}#g" $template

    # for reasons beyond by interest in investigating at the moment, secrets.token_urlsafe(48) produces a 64 character string and secrets.token_urlsafe(24) produces a 32 character string
    sed -i "s#RANDOM_64#$(python3 -c 'import secrets; print(secrets.token_urlsafe(48))')#g" $template
    sed -i "s#RANDOM_32#$(python3 -c 'import secrets; print(secrets.token_urlsafe(24))')#g" $template
done

podman network rm podman
podman network create podman

# Initialize PKI
wget --quiet -O /tmp/step-cli.deb https://dl.step.sm/gh-release/cli/gh-release-header/v0.19.0/step-cli_0.19.0_amd64.deb
sudo apt install -y /tmp/step-cli.deb

# Variables that should be configrable before launching the VM. Haven't figured out how to pass them through yet
PKI_NAME="Rhyzome Dev PKI"

mkdir -p "/etc/step-ca/secrets"

python3 -c 'import secrets; print(secrets.token_urlsafe(30))' > "/etc/step-ca/secrets/password"

localip="$(ip --json addr show | jq -r '.[] | select(.ifname == "ens2") | .addr_info[] | select(.family == "inet") | .local')"

chown -R 1000:1000 /etc/step-ca

# initialize the PKI
podman run --rm -v /etc/step-ca:/home/step smallstep/step-ca step ca init --deployment-type=standalone --ssh --name "${PKI_NAME}" --dns "ca.${domain}" --dns "${localip}" --dns "localhost" --address :8443 --provisioner jwks --password-file "secrets/password"

systemctl enable --now step-ca
sleep 1 # step-ca takes a second to come up

# fake a dns entry until the router comes up
echo "127.0.0.1  ca.${domain}" >> /etc/hosts

# because there is no mechanism to bring the router up yet, fake the dns entry after reboot
echo "127.0.0.1  ca.${domain}" >> /etc/cloud/templates/hosts.debian.tmpl

# Generate local certs
mkdir /etc/step # not needed after default config location for rhyzome-api moves here

fingerprint="$(step certificate fingerprint /etc/step-ca/certs/root_ca.crt)"
step ca bootstrap --ca-url https://ca.${domain}:8443 --fingerprint "${fingerprint}"

localip="$(ip --json addr show | jq -r '.[] | select(.ifname == "ens2") | .addr_info[] | select(.family == "inet") | .local')"
step ca certificate --provisioner-password-file /etc/step-ca/secrets/password --provisioner jwks --san "${localip}" "api.${domain}" /etc/step/api.crt /etc/step/api.key
systemctl enable --now cert-renewer@api.timer

mkdir -p /etc/rhyzome/api /etc/ssl/haproxy
/opt/cert-hook.sh

cp /etc/step-ca/certs/root_ca.crt /etc/step/ca.crt
chmod a+r /etc/step/ca.crt

# generate tokens so other members of the PKI can issue their own certs
node_id="$(generate_random_hex 5)"
router_id="$(generate_random_hex 5)"
curl -s -f -d "token=$(step ca token ${node_id} --san node.${domain} --not-after 1h --provisioner-password-file /etc/step-ca/secrets/password --provisioner jwks)" -d "fingerprint=${fingerprint}" "${2}/ca-available"
curl -s -f -d "token=$(step ca token ${router_id} --san router.${domain} --provisioner-password-file /etc/step-ca/secrets/password --provisioner jwks)" -d "fingerprint=${fingerprint}" "${1}/ca-available"

# setup rhyzome-api
set +x # avoid printing credentials (that dont get rotated) to screen
rhyzome_api_password="$(python3 -c 'import secrets; print(secrets.token_urlsafe(30))')"
echo POSTGRES_PASSWORD="${rhyzome_api_password}" > /etc/postgres.env
echo POSTGRES_DB=rhyzome >> /etc/postgres.env
chmod 600 /etc/postgres.env
python3 /opt/build-rhyzome-config.py "${fingerprint}" "${localip}" "${rhyzome_api_password}" > /etc/rhyzome/api/config.json
set -x

systemctl enable --now rhyzome-api-db
echo > /etc/postgres.env # once the password has been set, empty the file

# wait for postgres to start
until echo 'SELECT 1;' | sudo podman exec -i rhyzome-api-db psql -U postgres rhyzome; do sleep 1; done

# Download and start rhyzome services
podman exec -i rhyzome-api-db psql -U postgres rhyzome < /opt/rhyzome-schema.sql

mkdir -p /var/ory/hydra
chown 100 /var/ory/hydra

mkdir -p /var/ory/kratos
chown 10000 /var/ory/kratos

chown -R :101 /var/ory

systemctl enable --now haproxy
systemctl enable --now ory-hydra
systemctl enable --now ory-kratos
systemctl enable --now rhyzome-api
systemctl enable --now rhyzome-web

until echo "{\"credentials\":{\"password\":{\"config\":{\"password\":\"changeme\"}}},\"schema_id\":\"default\",\"traits\":{\"email\":\"admin@rhyzome.dev\"}}" | podman exec -i ory-kratos kratos import identities -e http://ory-kratos:4434; do
    sleep 1;
done

curl -s "${2}/setup-complete"
