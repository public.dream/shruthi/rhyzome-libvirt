#!/usr/bin/env python3
import json
import sys

ca_fp = sys.argv[1]
ip = sys.argv[2]
pg_password = sys.argv[3]


print(json.dumps({
    "provisioning_info": {
        "ca_fingerprint": ca_fp,
        "endpoints": {
            "grpc": "{}:9090".format(ip),
            "step": "https://{}:8443".format(ip)
        }
    },
    "db": f"dbname=rhyzome host=rhyzome-api-db password={pg_password} user=postgres sslmode=disable",
    "pki": {
        "cert": "/etc/rhyzome/api/api.crt",
        "key": "/etc/rhyzome/api/api.key",
        "ca": "/etc/step/ca.crt"
    }
}, indent=4))