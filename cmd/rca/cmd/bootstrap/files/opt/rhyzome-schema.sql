-- Copyright 2021 Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

CREATE TABLE tenants (
    name VARCHAR(20) UNIQUE NOT NULL, -- a unique identifier. This could be a lower case representation of the company name, or a randomly generated sequence of characters.
    state INT NOT NULL
);
INSERT INTO tenants (name, state) VALUES ('default', 0);

CREATE TABLE resources (
    resource_id VARCHAR(10) PRIMARY KEY UNIQUE NOT NULL,
    tenant VARCHAR(20) NOT NULL REFERENCES tenants(name)
);

CREATE TABLE resource_tags (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) NOT NULL,
    tag TEXT NOT NULL,
    value TEXT NOT NULL
);

CREATE TABLE nodes (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) UNIQUE NOT NULL,
    hostname VARCHAR(50) NOT NULL,
    available BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE instances (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) UNIQUE NOT NULL,
    base_image TEXT NOT NULL,
    memory INT NOT NULL,
    cpu INT NOT NULL,
    node VARCHAR(10) REFERENCES nodes(resource_id),
    state INT NOT NULL DEFAULT 0,
    user_data BYTEA,
    root_volume VARCHAR(10) REFERENCES resources(resource_id) NOT NULL
);

CREATE TABLE volumes (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) UNIQUE NOT NULL,
    node VARCHAR(10) REFERENCES nodes(resource_id) NOT NULL,
    size INT NOT NULL,
    instance VARCHAR(10) REFERENCES instances(resource_id)
);

CREATE TABLE jobs (
    job_id VARCHAR(10) PRIMARY KEY UNIQUE NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    assigned_worker VARCHAR(20) NOT NULL,
    state INT NOT NULL,
    state_text VARCHAR(20) NOT NULL,
    job_type VARCHAR(20) NOT NULL,
    job_data JSONB NOT NULL
);

CREATE TABLE openwrts (
    openwrt_id VARCHAR(10) UNIQUE NOT NULL
);

CREATE TABLE networks (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) UNIQUE NOT NULL,
    tenant VARCHAR(20) NOT NULL REFERENCES tenants(name),
    vlan_id INT NOT NULL -- TODO: think about rules (eg. less than 4096)
);

CREATE OR REPLACE FUNCTION notify_event() RETURNS TRIGGER AS $$
    DECLARE 
        data json;
        notification json;
    BEGIN
        -- Convert the old or new row to JSON, based on the kind of action.
        -- Action = DELETE?             -> OLD row
        -- Action = INSERT or UPDATE?   -> NEW row
        IF (TG_OP = 'DELETE') THEN
            data = row_to_json(OLD);
        ELSE
            data = row_to_json(NEW);
        END IF;
        -- Contruct the notification as a JSON string.
        notification = json_build_object('table', TG_TABLE_NAME, 'action', TG_OP, 'data', data);
        -- Execute pg_notify(channel, notification)
        PERFORM pg_notify(TG_TABLE_NAME, notification::text);
        -- Result is ignored since this is an AFTER trigger
        RETURN NULL; 
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER new_job_notify AFTER INSERT ON jobs FOR EACH ROW EXECUTE PROCEDURE notify_event();
CREATE TRIGGER new_instances_notify AFTER INSERT OR UPDATE OR DELETE ON instances FOR EACH ROW EXECUTE PROCEDURE notify_event();
