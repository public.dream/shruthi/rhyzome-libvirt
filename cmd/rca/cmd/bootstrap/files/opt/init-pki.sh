#!/bin/bash
# Copyright 2021 Entanglement Garden Developers
# SPDX-License-Identifier: AGPL-3.0-only
set -exuo pipefail

# Variables that should be configrable before launching the VM. Haven't figured out how to pass them through yet
PKI_NAME="Rhyzome Dev PKI"
PKI_CA_DOMAIN="ca.rhyzome.dev"

mkdir -p "${STEPPATH}/secrets"

# generate passwords to encrypt local secrets with
# no apparent security benefit as password is stored on
# the same machine. Maybe one day we can store this
# more securely while still being able to automatically
# load it at runtime
python3 -c 'import secrets; print(secrets.token_urlsafe(30))' > "${STEPPATH}/secrets/password"

localip="$(ip --json addr show | jq -r '.[] | select(.ifname != "lo") | .addr_info[] | select(.family == "inet") | .local')"

chown -R 1000:1000 /etc/step-ca

# initialize the PKI
podman run --rm -v /etc/step-ca:/home/step smallstep/step-ca step ca init --deployment-type=standalone --ssh --name "${PKI_NAME}" --dns "${PKI_CA_DOMAIN}" --dns "${localip}" --dns "localhost" --address :8443 --provisioner jwks --password-file "secrets/password"
