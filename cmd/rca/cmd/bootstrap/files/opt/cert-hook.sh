#!/bin/bash
set -exuo pipefail
cp /etc/step/api.crt /etc/step/api.key /etc/rhyzome/api
cat /etc/step/api.crt /etc/step/api.key > /etc/ssl/haproxy/api.pem
chown 99 /etc/ssl/haproxy/api.pem
if systemctl is-active haproxy; then
    systemctl reload haproxy
else
    systemctl restart haproxy
fi