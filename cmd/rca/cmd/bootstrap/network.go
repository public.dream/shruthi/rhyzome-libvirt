// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"fmt"
	"net"
	"strings"

	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/instances"
)

var (
	bootstrapNetworkLocalIP = "192.168.100.1" // be sure to update DHCP range below if any of the first three stanzas change
)

func getBridgeIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}

	bindIP := ""
	for _, i := range ifaces {
		if i.Name == config.C.BridgeInterface {
			addrs, err := i.Addrs()
			if err != nil {
				return "", err
			}
			bindIP = strings.Split(addrs[0].String(), "/")[0]
			break
		}
	}

	if bindIP == "" {
		return "", fmt.Errorf("could not find ip address for interface %s", config.C.BridgeInterface)
	}

	return bindIP, nil
}

func createLibvirtNetwork(conn *libvirt.Connect) error {
	network := libvirtxml.Network{
		Name: instances.BootstrapNetworkName,
		Forward: &libvirtxml.NetworkForward{
			Mode: "nat",
		},
		IPs: []libvirtxml.NetworkIP{
			libvirtxml.NetworkIP{
				Address: bootstrapNetworkLocalIP,
				DHCP: &libvirtxml.NetworkDHCP{
					Ranges: []libvirtxml.NetworkDHCPRange{
						libvirtxml.NetworkDHCPRange{
							Start: "192.168.100.128",
							End:   "192.168.100.254",
						},
					},
				},
			},
		},
	}
	networkXML, err := network.Marshal()
	if err != nil {
		return err
	}

	_, err = conn.NetworkCreateXML(networkXML)
	if err != nil {
		return err
	}

	return nil
}
