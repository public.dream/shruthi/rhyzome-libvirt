// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var bootstrapGenerateCmd = &cobra.Command{
	Use: "generate",
	Run: func(_ *cobra.Command, _ []string) {
		wd, err := os.Getwd()
		if err != nil {
			log.Fatal("error looking up cwd: ", err)
		}

		base := filepath.Join(wd, "cmd", "rca", "cmd", "bootstrap", "files")

		output := `// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

// This file is generated! Do not attempt to overwrite manually! To update, edit files/ and run:
// go run ./cmd/rca bootstrap generate


var cloudConfigWriteFiles = []cloudConfigFile{`

		err = filepath.Walk(base, func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				log.Fatal("error walking through file path: ", err)
			}

			stat, err := os.Stat(path)
			if err != nil {
				return err
			}

			if stat.IsDir() {
				return nil // skip over directories
			}

			content, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}

			relativePath := strings.ReplaceAll(path, base, "")

			permissions := "0644"
			if strings.HasSuffix(path, ".sh") {
				permissions = "0755"
			}

			output = fmt.Sprintf("%s\ncloudConfigFile{\nPath: \"%s\",\nContent: `%s`,\nPermissions: \"%s\",\n},", output, relativePath, content, permissions)
			return nil
		})
		if err != nil {
			log.Fatal("error reading files: ", err)
		}

		output = fmt.Sprintf("%s\n}\n", output)

		generatedfilename := filepath.Join(base, "..", "cloudinit.gen.go")

		f, err := os.Create(generatedfilename)
		if err != nil {
			log.Fatal("error creating file ", generatedfilename, ": ", err)
		}
		defer f.Close()
		_, err = f.Write([]byte(output))
		if err != nil {
			log.Fatal("error: ", err)
		}
		f.Close()

		fmt := exec.Command("go", "fmt", "-x", generatedfilename)
		if err := fmt.Run(); err != nil {
			log.Fatal("error running go fmt -x ", generatedfilename, ": ", err)
		}
	},
}

func init() {
	BootstrapCmd.AddCommand(bootstrapGenerateCmd)
}
