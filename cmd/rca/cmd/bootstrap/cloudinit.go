// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

type cloudConfigStruct struct {
	Packages          []string          `yaml:"packages"`
	SSHAuthorizedKeys []string          `yaml:"ssh_authorized_keys"`
	WriteFiles        []cloudConfigFile `yaml:"write_files"`
	RunCmd            [][]string        `yaml:"runcmd"`
}

type cloudConfigFile struct {
	Content     string `yaml:"content"`
	Path        string `yaml:"path"`
	Permissions string `yaml:"permissions"`
}

func (c *cloudConfigStruct) WriteFile(file cloudConfigFile) {
	c.WriteFiles = append(c.WriteFiles, file)
}

var (
	cloudConfig = cloudConfigStruct{
		Packages:          []string{"podman", "jq", "tmux", "mosh", "htop"},
		SSHAuthorizedKeys: []string{},
		WriteFiles:        cloudConfigWriteFiles,
	}
)

func readLocalSSHKey() {
	home, _ := os.UserHomeDir()
	if key, err := ioutil.ReadFile(filepath.Join(home, ".ssh", "id_ed25519.pub")); err == nil {
		keystring := strings.TrimSpace(string(key))
		log.Debug("injecting SSH key into apiserver VM: ", keystring)
		cloudConfig.SSHAuthorizedKeys = append(cloudConfig.SSHAuthorizedKeys, keystring)
	} else if key, err := ioutil.ReadFile(filepath.Join(home, ".ssh", "id_rsa.pub")); err == nil {
		keystring := strings.TrimSpace(string(key))
		log.Debug("injecting SSH key into apiserver VM: ", keystring)
		cloudConfig.SSHAuthorizedKeys = append(cloudConfig.SSHAuthorizedKeys, keystring)
	}
}
