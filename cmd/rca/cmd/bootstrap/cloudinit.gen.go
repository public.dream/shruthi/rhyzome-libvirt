// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

// This file is generated! Do not attempt to overwrite manually! To update, edit files/ and run:
// go run ./cmd/rca bootstrap generate

var cloudConfigWriteFiles = []cloudConfigFile{
	cloudConfigFile{
		Path: "/etc/haproxy/haproxy.cfg",
		Content: `global
	log stdout format raw local0
	# stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
	# stats timeout 30s

	# Default SSL material locations
	ca-base /etc/ssl/certs
	crt-base /etc/ssl/haproxy

	# See: https://ssl-config.mozilla.org/#server=haproxy&server-version=2.0.3&config=intermediate
	ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
	ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
	ssl-default-bind-options ssl-min-ver TLSv1.2 no-tls-tickets

defaults
	log	global
	mode	http
	option	httplog
	option	dontlognull
	timeout connect 5000
	timeout client  50000
	timeout server  50000

	# never fail on address resolution
	default-server check inter 30 rise 1 fall 1 resolvers resolvconf init-addr last,libc,none

frontend http
	bind	:8443 ssl crt /etc/ssl/haproxy
	log global
	# HSTS (15768000 seconds = 6 month
	http-response set-header Strict-Transport-Security max-age=15768000

	capture request header Referrer len 64
	capture request header Content-Length len 10
	capture request header User-Agent len 64

	use_backend kratos        if { hdr_beg(host) -i kratos.DOMAIN }
	use_backend rhyzome_web   if { hdr_beg(host) -i console.DOMAIN }
	use_backend rhyzome_api

resolvers resolvconf
	parse-resolv-conf
	resolve_retries 3
	timeout retry 500ms

backend kratos
	server kratos ory-kratos:4433

backend rhyzome_api
	server localhost rhyzome-api:8080

backend rhyzome_web
	server rhyzome-web rhyzome-web:8085
`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/ory/hydra.yml",
		Content: `serve:
  cookies:
    same_site_mode: Lax

urls:
  self:
    issuer: https://hydra.DOMAIN
  consent: https://kratos-ui.DOMAIN/consent
  login: https://kratos-ui.DOMAIN/login
  logout: https://kratos-ui.DOMAIN/logout

secrets:
  system:
    - RANDOM_64

oidc:
  subject_identifiers:
    supported_types:
      - pairwise
      - public
    pairwise:
      salt: OIDC_SALT
`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/ory/kratos/identity.schema.json",
		Content: `{
    "$id": "https://schemas.ory.sh/presets/kratos/quickstart/email-password/identity.schema.json",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Person",
    "type": "object",
    "properties": {
      "traits": {
        "type": "object",
        "properties": {
          "email": {
            "type": "string",
            "format": "email",
            "title": "E-Mail",
            "minLength": 3,
            "ory.sh/kratos": {
              "credentials": {
                "password": {
                  "identifier": true
                }
              },
              "verification": {
                "via": "email"
              },
              "recovery": {
                "via": "email"
              }
            }
          },
          "name": {
            "type": "object",
            "properties": {
              "first": {
                "title": "First Name",
                "type": "string"
              },
              "last": {
                "title": "Last Name",
                "type": "string"
              }
            }
          }
        },
        "required": [
          "email"
        ],
        "additionalProperties": false
      }
    }
  }
  `,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/ory/kratos/kratos.yml",
		Content: `version: v0.7.1-alpha.1

dsn: sqlite:///var/ory/kratos/kratos.db?_fk=true

serve:
  public:
    base_url: https://kratos.DOMAIN/
    cors:
      enabled: true
  admin:
    base_url: https://kratos-admin.DOMAIN/

selfservice:
  default_browser_return_url: https://console.DOMAIN
  allowed_return_urls:
    - https://console.DOMAIN

  methods:
    password:
      enabled: true

  flows:
    error:
      ui_url: https://console.DOMAIN/auth/error

    settings:
      ui_url: https://console.DOMAIN/settings
      privileged_session_max_age: 15m

    recovery:
      enabled: true
      ui_url: https://console.DOMAIN/auth/recovery

    verification:
      enabled: true
      ui_url: https://console.DOMAIN/auth/verification
      after:
        default_browser_return_url: https://console.DOMAIN/

    logout:
      after:
        default_browser_return_url: https://console.DOMAIN/auth/login

    login:
      ui_url: https://console.DOMAIN/auth/login
      lifespan: 10m

    registration:
      lifespan: 10m
      ui_url: https://console.DOMAIN/auth/registration
      after:
        password:
          hooks:
            -
              hook: session

log:
  level: debug
  format: text
  leak_sensitive_values: true

secrets:
  cookie:
    - RANDOM_64
  cipher:
    - RANDOM_32

ciphers:
  algorithm: xchacha20-poly1305

hashers:
  algorithm: bcrypt
  bcrypt:
    cost: 8

identity:
  default_schema_id: default
  schemas:
    - id: default
      url: file:///etc/config/kratos/identity.schema.json

courier:
  smtp:
    connection_uri: smtps://test:test@mailslurper:1025/?skip_ssl_verify=true

cookies:
  domain: DOMAIN`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/rhyzome/web.json",
		Content: `{
    "api_endpoint": "http://rhyzome-api:8080",
    "kratos_public_domain": "https://kratos.DOMAIN",
    "kratos": {
        "URL": "http://ory-kratos:4433"
    },
    "kratos_admin": {
        "URL": "http://ory-kratos:4434"
    }
}`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/cert-renewer@.service",
		Content: `[Unit]
Description=Certificate renewer for %I
After=network-online.target
Documentation=https://smallstep.com/docs/step-ca/certificate-authority-server-production
StartLimitIntervalSec=0

[Service]
Type=oneshot
User=root

Environment=STEPPATH=/etc/step-ca \
            CERT_LOCATION=/etc/step/%i.crt \
            KEY_LOCATION=/etc/step/%i.key

; ExecCondition checks if the certificate is ready for renewal,
; based on the exit status of the command.
; (In systemd 242 or below, you can use ExecStartPre= here.)
ExecCondition=/usr/bin/step certificate needs-renewal ${CERT_LOCATION}

; ExecStart renews the certificate, if ExecStartPre was successful.
ExecStart=/usr/bin/step ca renew --force ${CERT_LOCATION} ${KEY_LOCATION}

[Install]
WantedBy=multi-user.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/cert-renewer@.timer",
		Content: `[Unit]
Description=Certificate renewal timer for %I
Documentation=https://smallstep.com/docs/step-ca/certificate-authority-server-production

[Timer]
Persistent=true

; Run the timer unit every 5 minutes.
OnCalendar=*:1/5

; Always run the timer on time.
AccuracySec=1us

; Add jitter to prevent a "thundering hurd" of simultaneous certificate renewals.
RandomizedDelaySec=5m

[Install]
WantedBy=timers.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/cert-renewer@api.service.d/override.conf",
		Content: `[Service]
ExecStartPost=/opt/cert-hook.sh`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/haproxy.service",
		Content: `[Unit]
Description=haproxy
Wants=network-online.target
After=network-online.target
RequiresMountsFor=%t/containers

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStopSec=70
ExecStartPre=/bin/rm -f %t/%n.ctr-id
# Note: using haproxy 2.5 as 2.6 (latest stable at time of writing) runs at 100% CPU usage for unknown reasons. Debug it if you want.
ExecStart=/usr/bin/podman run --cidfile=%t/%n.ctr-id --cgroups=no-conmon --rm --sdnotify=conmon -d --replace --name haproxy -p 443:8443 -p 80:80 -v /etc/haproxy:/usr/local/etc/haproxy:ro -v /etc/ssl:/etc/ssl:ro library/haproxy:2.5
ExecStop=/usr/bin/podman stop --ignore --cidfile=%t/%n.ctr-id
ExecStopPost=/usr/bin/podman rm -f --ignore --cidfile=%t/%n.ctr-id
Type=notify
NotifyAccess=all

[Install]
WantedBy=default.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/ory-hydra.service",
		Content: `[Unit]
Description=Ory Hydra
Wants=network-online.target
After=network-online.target
RequiresMountsFor=%t/containers

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStopSec=70
ExecStartPre=/bin/rm -f %t/%n.ctr-id
ExecStart=/usr/bin/podman run --cidfile=%t/%n.ctr-id --cgroups=no-conmon --rm --sdnotify=conmon -d --replace --name ory-hydra -p 4444 -p 4445 -p 5555 -v /etc/ory/hydra.yml:/etc/config/hydra/hydra.yml:ro -v /var/ory/hydra:/var/ory/hydra --env-host oryd/hydra serve -c /etc/config/hydra/hydra.yml all --dangerous-force-http
ExecStop=/usr/bin/podman stop --ignore --cidfile=%t/%n.ctr-id
ExecStopPost=/usr/bin/podman rm -f --ignore --cidfile=%t/%n.ctr-id
Type=notifyNotifyAccess=all

[Install]
WantedBy=default.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/ory-kratos.service",
		Content: `[Unit]
Description=Ory Kratos
Wants=network-online.target
After=network-online.target
RequiresMountsFor=%t/containers

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStopSec=70
ExecStartPre=/bin/rm -f %t/%n.ctr-id
ExecStartPre=/usr/bin/podman run --cgroups=no-conmon --rm --sdnotify=conmon --name ory-kratos-migrate -e DSN=sqlite:///var/ory/kratos/kratos.db?_fk=true -v /etc/ory/kratos:/etc/config/kratos:ro -v /var/ory/kratos:/var/ory/kratos oryd/kratos migrate sql -e --yes
ExecStart=/usr/bin/podman run --cidfile=%t/%n.ctr-id --cgroups=no-conmon --rm --sdnotify=conmon -d --replace --name ory-kratos -p 4444 -p 4445 -p 5555 -e DSN=sqlite:///var/ory/kratos/kratos.db?_fk=true -v /etc/ory/kratos:/etc/config/kratos:ro -v /var/ory/kratos:/var/ory/kratos --env-host oryd/kratos serve -c /etc/config/kratos/kratos.yml --dev --watch-courier
ExecStop=/usr/bin/podman stop --ignore --cidfile=%t/%n.ctr-id
ExecStopPost=/usr/bin/podman rm -f --ignore --cidfile=%t/%n.ctr-id
Type=notify
NotifyAccess=all

[Install]
WantedBy=default.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/rhyzome-api-db.service",
		Content: `[Unit]
Description=rhyzome-api-db
Wants=network-online.target
After=network-online.target
RequiresMountsFor=%t/containers

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStopSec=70
ExecStartPre=/bin/rm -f %t/%n.ctr-id
ExecStart=/usr/bin/podman run --cidfile=%t/%n.ctr-id --cgroups=no-conmon --rm --sdnotify=conmon -d --replace --env-file /etc/postgres.env -v rhyzome-api-pg:/var/lib/postgresql/data --name rhyzome-api-db library/postgres
ExecStop=/usr/bin/podman stop --ignore --cidfile=%t/%n.ctr-id
ExecStopPost=/usr/bin/podman rm -f --ignore --cidfile=%t/%n.ctr-id
Type=notify
NotifyAccess=all

[Install]
WantedBy=default.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/rhyzome-api.service",
		Content: `[Unit]
Description=Rhyzome API
Wants=network-online.target
After=network-online.target
RequiresMountsFor=%t/containers

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStopSec=70
ExecStartPre=/bin/rm -f %t/%n.ctr-id
ExecStart=/usr/bin/podman run --cidfile=%t/%n.ctr-id --cgroups=no-conmon --rm --sdnotify=conmon -d --replace --name rhyzome-api -v /etc/step/ca.crt:/etc/step/ca.crt -v /etc/rhyzome/api/config.json:/etc/rhyzome-api.json:ro -v /etc/rhyzome/api:/etc/rhyzome/api:ro -p 9090:9090 registry.git.callpipe.com/entanglement.garden/rhyzome-api:71e752b735ce09c6ed500b92fe01499fb1a52772
ExecStop=/usr/bin/podman stop --ignore --cidfile=%t/%n.ctr-id
ExecStopPost=/usr/bin/podman rm -f --ignore --cidfile=%t/%n.ctr-id
Type=notify
NotifyAccess=all

[Install]
WantedBy=default.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/rhyzome-web.service",
		Content: `[Unit]
Description=Rhyzome Web
Wants=network-online.target
After=network-online.target
RequiresMountsFor=%t/containers

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStopSec=70
ExecStartPre=/bin/rm -f %t/%n.ctr-id
ExecStart=/usr/bin/podman run --cidfile=%t/%n.ctr-id --cgroups=no-conmon --rm --sdnotify=conmon -d --replace --name rhyzome-web -v /etc/rhyzome/web.json:/etc/rhyzome-web.json:ro registry.git.callpipe.com/entanglement.garden/rhyzome-web:f4f75fb3095e9c71fc74592919df4cd84b2bed85
ExecStop=/usr/bin/podman stop --ignore --cidfile=%t/%n.ctr-id
ExecStopPost=/usr/bin/podman rm -f --ignore --cidfile=%t/%n.ctr-id
Type=notify
NotifyAccess=all

[Install]
WantedBy=default.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/etc/systemd/system/step-ca.service",
		Content: `[Unit]
Description=step-ca service
Documentation=https://smallstep.com/docs/step-ca
Documentation=https://smallstep.com/docs/step-ca/certificate-authority-server-production
Wants=network-online.target
After=network-online.target
RequiresMountsFor=%t/containers

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStopSec=70ExecStartPre=/bin/rm -f %t/%n.ctr-id
ExecStart=/usr/bin/podman run --cidfile=%t/%n.ctr-id --cgroups=no-conmon --rm --sdnotify=conmon -d --replace --name step-ca -p 8443:8443 -v /etc/step-ca:/home/step smallstep/step-ca
ExecStop=/usr/bin/podman stop --ignore --cidfile=%t/%n.ctr-id
ExecStopPost=/usr/bin/podman rm -f --ignore --cidfile=%t/%n.ctr-id
Type=notify
NotifyAccess=all

[Install]
WantedBy=default.target`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/opt/bootstrap.sh",
		Content: `#!/bin/bash
# Copyright 2021 Entanglement Garden Developers
# SPDX-License-Identifier: AGPL-3.0-only
set -exuo pipefail

function generate_random() {
    python3 -c "import secrets; print(secrets.token_urlsafe($1))"
}

function generate_random_hex() {
    python3 -c "import secrets; print(secrets.token_hex($1))"
}

domain=${3}
for template in /etc/haproxy/haproxy.cfg /etc/ory/kratos/kratos.yml /etc/ory/hydra.yml /etc/rhyzome/web.json; do
    sed -i "s#DOMAIN#${domain}#g" $template

    # for reasons beyond by interest in investigating at the moment, secrets.token_urlsafe(48) produces a 64 character string and secrets.token_urlsafe(24) produces a 32 character string
    sed -i "s#RANDOM_64#$(python3 -c 'import secrets; print(secrets.token_urlsafe(48))')#g" $template
    sed -i "s#RANDOM_32#$(python3 -c 'import secrets; print(secrets.token_urlsafe(24))')#g" $template
done

podman network rm podman
podman network create podman

# Initialize PKI
wget --quiet -O /tmp/step-cli.deb https://dl.step.sm/gh-release/cli/gh-release-header/v0.19.0/step-cli_0.19.0_amd64.deb
sudo apt install -y /tmp/step-cli.deb

# Variables that should be configrable before launching the VM. Haven't figured out how to pass them through yet
PKI_NAME="Rhyzome Dev PKI"

mkdir -p "/etc/step-ca/secrets"

python3 -c 'import secrets; print(secrets.token_urlsafe(30))' > "/etc/step-ca/secrets/password"

localip="$(ip --json addr show | jq -r '.[] | select(.ifname == "ens2") | .addr_info[] | select(.family == "inet") | .local')"

chown -R 1000:1000 /etc/step-ca

# initialize the PKI
podman run --rm -v /etc/step-ca:/home/step smallstep/step-ca step ca init --deployment-type=standalone --ssh --name "${PKI_NAME}" --dns "ca.${domain}" --dns "${localip}" --dns "localhost" --address :8443 --provisioner jwks --password-file "secrets/password"

systemctl enable --now step-ca
sleep 1 # step-ca takes a second to come up

# fake a dns entry until the router comes up
echo "127.0.0.1  ca.${domain}" >> /etc/hosts

# because there is no mechanism to bring the router up yet, fake the dns entry after reboot
echo "127.0.0.1  ca.${domain}" >> /etc/cloud/templates/hosts.debian.tmpl

# Generate local certs
mkdir /etc/step # not needed after default config location for rhyzome-api moves here

fingerprint="$(step certificate fingerprint /etc/step-ca/certs/root_ca.crt)"
step ca bootstrap --ca-url https://ca.${domain}:8443 --fingerprint "${fingerprint}"

localip="$(ip --json addr show | jq -r '.[] | select(.ifname == "ens2") | .addr_info[] | select(.family == "inet") | .local')"
step ca certificate --provisioner-password-file /etc/step-ca/secrets/password --provisioner jwks --san "${localip}" "api.${domain}" /etc/step/api.crt /etc/step/api.key
systemctl enable --now cert-renewer@api.timer

mkdir -p /etc/rhyzome/api /etc/ssl/haproxy
/opt/cert-hook.sh

cp /etc/step-ca/certs/root_ca.crt /etc/step/ca.crt
chmod a+r /etc/step/ca.crt

# generate tokens so other members of the PKI can issue their own certs
node_id="$(generate_random_hex 5)"
router_id="$(generate_random_hex 5)"
curl -s -f -d "token=$(step ca token ${node_id} --san node.${domain} --not-after 1h --provisioner-password-file /etc/step-ca/secrets/password --provisioner jwks)" -d "fingerprint=${fingerprint}" "${2}/ca-available"
curl -s -f -d "token=$(step ca token ${router_id} --san router.${domain} --provisioner-password-file /etc/step-ca/secrets/password --provisioner jwks)" -d "fingerprint=${fingerprint}" "${1}/ca-available"

# setup rhyzome-api
set +x # avoid printing credentials (that dont get rotated) to screen
rhyzome_api_password="$(python3 -c 'import secrets; print(secrets.token_urlsafe(30))')"
echo POSTGRES_PASSWORD="${rhyzome_api_password}" > /etc/postgres.env
echo POSTGRES_DB=rhyzome >> /etc/postgres.env
chmod 600 /etc/postgres.env
python3 /opt/build-rhyzome-config.py "${fingerprint}" "${localip}" "${rhyzome_api_password}" > /etc/rhyzome/api/config.json
set -x

systemctl enable --now rhyzome-api-db
echo > /etc/postgres.env # once the password has been set, empty the file

# wait for postgres to start
until echo 'SELECT 1;' | sudo podman exec -i rhyzome-api-db psql -U postgres rhyzome; do sleep 1; done

# Download and start rhyzome services
podman exec -i rhyzome-api-db psql -U postgres rhyzome < /opt/rhyzome-schema.sql

mkdir -p /var/ory/hydra
chown 100 /var/ory/hydra

mkdir -p /var/ory/kratos
chown 10000 /var/ory/kratos

chown -R :101 /var/ory

systemctl enable --now haproxy
systemctl enable --now ory-hydra
systemctl enable --now ory-kratos
systemctl enable --now rhyzome-api
systemctl enable --now rhyzome-web

until echo "{\"credentials\":{\"password\":{\"config\":{\"password\":\"changeme\"}}},\"schema_id\":\"default\",\"traits\":{\"email\":\"admin@rhyzome.dev\"}}" | podman exec -i ory-kratos kratos import identities -e http://ory-kratos:4434; do
    sleep 1;
done

curl -s "${2}/setup-complete"
`,
		Permissions: "0755",
	},
	cloudConfigFile{
		Path: "/opt/build-rhyzome-config.py",
		Content: `#!/usr/bin/env python3
import json
import sys

ca_fp = sys.argv[1]
ip = sys.argv[2]
pg_password = sys.argv[3]


print(json.dumps({
    "provisioning_info": {
        "ca_fingerprint": ca_fp,
        "endpoints": {
            "grpc": "{}:9090".format(ip),
            "step": "https://{}:8443".format(ip)
        }
    },
    "db": f"dbname=rhyzome host=rhyzome-api-db password={pg_password} user=postgres sslmode=disable",
    "pki": {
        "cert": "/etc/rhyzome/api/api.crt",
        "key": "/etc/rhyzome/api/api.key",
        "ca": "/etc/step/ca.crt"
    }
}, indent=4))`,
		Permissions: "0644",
	},
	cloudConfigFile{
		Path: "/opt/cert-hook.sh",
		Content: `#!/bin/bash
set -exuo pipefail
cp /etc/step/api.crt /etc/step/api.key /etc/rhyzome/api
cat /etc/step/api.crt /etc/step/api.key > /etc/ssl/haproxy/api.pem
chown 99 /etc/ssl/haproxy/api.pem
if systemctl is-active haproxy; then
    systemctl reload haproxy
else
    systemctl restart haproxy
fi`,
		Permissions: "0755",
	},
	cloudConfigFile{
		Path: "/opt/init-pki.sh",
		Content: `#!/bin/bash
# Copyright 2021 Entanglement Garden Developers
# SPDX-License-Identifier: AGPL-3.0-only
set -exuo pipefail

# Variables that should be configrable before launching the VM. Haven't figured out how to pass them through yet
PKI_NAME="Rhyzome Dev PKI"
PKI_CA_DOMAIN="ca.rhyzome.dev"

mkdir -p "${STEPPATH}/secrets"

# generate passwords to encrypt local secrets with
# no apparent security benefit as password is stored on
# the same machine. Maybe one day we can store this
# more securely while still being able to automatically
# load it at runtime
python3 -c 'import secrets; print(secrets.token_urlsafe(30))' > "${STEPPATH}/secrets/password"

localip="$(ip --json addr show | jq -r '.[] | select(.ifname != "lo") | .addr_info[] | select(.family == "inet") | .local')"

chown -R 1000:1000 /etc/step-ca

# initialize the PKI
podman run --rm -v /etc/step-ca:/home/step smallstep/step-ca step ca init --deployment-type=standalone --ssh --name "${PKI_NAME}" --dns "${PKI_CA_DOMAIN}" --dns "${localip}" --dns "localhost" --address :8443 --provisioner jwks --password-file "secrets/password"
`,
		Permissions: "0755",
	},
	cloudConfigFile{
		Path: "/opt/rhyzome-schema.sql",
		Content: `-- Copyright 2021 Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

CREATE TABLE tenants (
    name VARCHAR(20) UNIQUE NOT NULL, -- a unique identifier. This could be a lower case representation of the company name, or a randomly generated sequence of characters.
    state INT NOT NULL
);
INSERT INTO tenants (name, state) VALUES ('default', 0);

CREATE TABLE resources (
    resource_id VARCHAR(10) PRIMARY KEY UNIQUE NOT NULL,
    tenant VARCHAR(20) NOT NULL REFERENCES tenants(name)
);

CREATE TABLE resource_tags (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) NOT NULL,
    tag TEXT NOT NULL,
    value TEXT NOT NULL
);

CREATE TABLE nodes (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) UNIQUE NOT NULL,
    hostname VARCHAR(50) NOT NULL,
    available BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE instances (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) UNIQUE NOT NULL,
    base_image TEXT NOT NULL,
    memory INT NOT NULL,
    cpu INT NOT NULL,
    node VARCHAR(10) REFERENCES nodes(resource_id),
    state INT NOT NULL DEFAULT 0,
    user_data BYTEA,
    root_volume VARCHAR(10) REFERENCES resources(resource_id) NOT NULL
);

CREATE TABLE volumes (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) UNIQUE NOT NULL,
    node VARCHAR(10) REFERENCES nodes(resource_id) NOT NULL,
    size INT NOT NULL,
    instance VARCHAR(10) REFERENCES instances(resource_id)
);

CREATE TABLE jobs (
    job_id VARCHAR(10) PRIMARY KEY UNIQUE NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    assigned_worker VARCHAR(20) NOT NULL,
    state INT NOT NULL,
    state_text VARCHAR(20) NOT NULL,
    job_type VARCHAR(20) NOT NULL,
    job_data JSONB NOT NULL
);

CREATE TABLE openwrts (
    openwrt_id VARCHAR(10) UNIQUE NOT NULL
);

CREATE TABLE networks (
    resource_id VARCHAR(10) REFERENCES resources(resource_id) UNIQUE NOT NULL,
    tenant VARCHAR(20) NOT NULL REFERENCES tenants(name),
    vlan_id INT NOT NULL -- TODO: think about rules (eg. less than 4096)
);

CREATE OR REPLACE FUNCTION notify_event() RETURNS TRIGGER AS $$
    DECLARE 
        data json;
        notification json;
    BEGIN
        -- Convert the old or new row to JSON, based on the kind of action.
        -- Action = DELETE?             -> OLD row
        -- Action = INSERT or UPDATE?   -> NEW row
        IF (TG_OP = 'DELETE') THEN
            data = row_to_json(OLD);
        ELSE
            data = row_to_json(NEW);
        END IF;
        -- Contruct the notification as a JSON string.
        notification = json_build_object('table', TG_TABLE_NAME, 'action', TG_OP, 'data', data);
        -- Execute pg_notify(channel, notification)
        PERFORM pg_notify(TG_TABLE_NAME, notification::text);
        -- Result is ignored since this is an AFTER trigger
        RETURN NULL; 
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER new_job_notify AFTER INSERT ON jobs FOR EACH ROW EXECUTE PROCEDURE notify_event();
CREATE TRIGGER new_instances_notify AFTER INSERT OR UPDATE OR DELETE ON instances FOR EACH ROW EXECUTE PROCEDURE notify_event();
`,
		Permissions: "0644",
	},
}
