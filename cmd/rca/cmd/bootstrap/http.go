// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

var (
	metadataPort        = 8080
	routerBootstrapData []byte
)

func httpListener(addr string) {
	http.HandleFunc("/router-bootstrap-data.json", func(w http.ResponseWriter, r *http.Request) {
		if _, err := w.Write(routerBootstrapData); err != nil {
			log.Error("error writing http response: ", err)
		}
	})

	http.HandleFunc("/setup-complete", func(w http.ResponseWriter, r *http.Request) {
		if _, err := w.Write([]byte{}); err != nil {
			log.Error("error writing http response: ", err)
		}
		go func() {
			time.Sleep(time.Second)
			log.Info("bootstrapping complete, your cluster is now running!")
			log.Info("  Console Access:")
			log.Infof("    URL:       https://console.%s", dns)
			log.Info("    ID:        admin@rhyzome.dev")
			log.Info("    password:  changeme")
			os.Exit(0)
		}()
	})

	http.HandleFunc("/ca-available", func(w http.ResponseWriter, r *http.Request) {
		fingerprint := r.PostFormValue("fingerprint")
		token := r.PostFormValue("token")
		remoteIP := strings.SplitN(r.RemoteAddr, ":", 2)[0]

		joinurl := fmt.Sprintf("rhyzome://%s/?ca=%s&token=%s", remoteIP, fingerprint, token)

		if joinTokenFilename != "" {
			f, err := os.Create(joinTokenFilename)
			if err != nil {
				log.Error(err)
				http.Error(w, err.Error(), 500)
				return
			}

			if _, err := f.Write([]byte(joinurl)); err != nil {
				log.Error("error writing to ", joinTokenFilename, ": ", err)
			}
			log.Info("wrote join token to ", joinTokenFilename)
		}

		log.Info("to join new cluster: sudo rca join \"", joinurl, "\"")
	})

	log.Debug("starting metadata http server on ", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatal("error starting http listener on ", addr, ":", err)
	}
}
