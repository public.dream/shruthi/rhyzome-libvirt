// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"log/syslog"
	"os"

	"github.com/sirupsen/logrus"
	lSyslog "github.com/sirupsen/logrus/hooks/syslog"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/grpcclient"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/instances"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

// see https://www.libvirt.org/hooks.html#arguments

func main() {
	log := logrus.New()
	hook, err := lSyslog.NewSyslogHook("", "", syslog.LOG_INFO, "rhyzome-libvirt-hook-qemu")

	if err == nil {
		log.Hooks.Add(hook)
	}

	if len(os.Args) != 5 {
		log.Error("incorrect number of arguments")
		return
	}
	config.Load()
	if err := grpcclient.ConnectOneShot(); err != nil {
		log.Error("error connecting to grpc server: ", err)
		return
	}

	defer func() { _ = grpcclient.Shutdown() }()

	guestName := os.Args[1]
	switch os.Args[2] { // operation
	case "started":
		req := &rhyzome_protos.InstanceStartedRequest{Id: instances.DomainNameToId(guestName)}
		_, err = grpcclient.LibvirtClient.InstanceStarted(context.Background(), req)
		if err != nil {
			log.Error("error submitting instance started event: ", err)
			return
		}
		log.Info("marked instance started")
	case "stopped":
		req := &rhyzome_protos.InstanceStoppedRequest{Id: instances.DomainNameToId(guestName)}
		_, err = grpcclient.LibvirtClient.InstanceStopped(context.Background(), req)
		if err != nil {
			log.Error("error submitting instance stopped event: ", err)
			return
		}
		log.Info("marked instance stopped")
	default:
		log.Debug("ignored operation")
	}
}
