// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-libvirt/grpcclient"
)

var signals = make(chan os.Signal, 1)

func main() {
	signal.Notify(signals, syscall.SIGINT, syscall.SIGHUP)

	config.Load()

	// static_instances.EnsureStaticInstances()

	log.Info("connecting")
	go grpcclient.Connect()

	for {
		signal := <-signals
		log.Debug("received signal", signal)
		switch signal {
		case syscall.SIGHUP:
			config.Load()
		case syscall.SIGINT:
			err := grpcclient.Shutdown()
			if err != nil {
				log.Error("error shutting down grpc client: ", err)
			}
			return
		}
	}
}
