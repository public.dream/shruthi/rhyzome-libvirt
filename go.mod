module git.callpipe.com/entanglement.garden/rhyzome-libvirt

go 1.15

require (
	git.callpipe.com/entanglement.garden/rhyzome-openapi v0.0.0-20220502192255-b41d9991a224
	git.callpipe.com/entanglement.garden/rhyzome-protos v0.0.0-20210918060153-2d0fa58289c4
	github.com/golang-jwt/jwt/v4 v4.4.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.4.0
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8
	google.golang.org/genproto v0.0.0-20200806141610-86f49bd18e98 // indirect
	google.golang.org/grpc v1.40.0
	gopkg.in/yaml.v2 v2.4.0
	libvirt.org/go/libvirt v1.8005.0
	libvirt.org/go/libvirtxml v1.8005.0
)
