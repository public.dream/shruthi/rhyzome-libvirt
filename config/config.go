// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"
)

var ConfigFiles = []string{"/etc/rhyzome/libvirt.json", "rhyzome-libvirt.json"}

// Config describes all configurable keys
type Config struct {
	Bind            string `json:"bind,omitempty"`              // Bind is the address (and port) to bind the GRPC server to
	BridgeInterface string `json:"bridge_interface,omitempty"`  // BridgeInterface is the bridge that all network interfaces are added to
	WANInterface    string `json:"wan_interface,omitempty"`     // WANInterface is the network bridge for the WAN interface
	DiskStoragePool string `json:"disk_storage_pool,omitempty"` // DiskStoragePool is the name of the storage pool to use
	ImageDir        string `json:"image_dir,omitempty"`         // ImageDir is the path to the local image pool
	ImageCache      string `json:"image_cache,omitempty"`       // ImageCache is the path on the local disk to a folder where images should be cached
	ImageHost       string `json:"imagehost,omitempty"`         // ImageHost is the base URL for the disk image server
	PKI             PKI    `json:"pki,omitempty"`
	MetadataURL     string `json:"metadata_url,omitempty"`
	APIServer       string `json:"apiserver,omitempty"`        // The host:port of the central API server to connect to
	StaticInstances string `json:"static_instances,omitempty"` // path to directory to read static instances from
	MetadataBind    string `json:"metadata_bind,omitempty"`    // MetadataBind is the host and port to bind the metadata server to
}

// PKI can load certs off the disk or allow them to be configured at runtime (used for cluster bootstrapping)
type PKI struct {
	Cert    string `json:"cert,omitempty"`
	rawCert []byte
	Key     string `json:"key,omitempty"`
	rawKey  []byte
	CA      string `json:"ca,omitempty"`
	rawCA   []byte
}

// SetRaw sets the raw values of the key, cert and CA as byte arrays
func (p *PKI) SetRaw(cert, key, ca []byte) {
	p.rawCert = cert
	p.rawKey = key
	p.rawCA = ca
}

// GetX509KeyPair read the x509 key pair from disk or from the raw values if set
func (p PKI) GetX509KeyPair() (tls.Certificate, error) {
	if len(p.rawCert) > 0 && len(p.rawKey) > 0 {
		return tls.X509KeyPair(p.rawCert, p.rawKey)
	}

	log.Debug("loading x509 pair from ", p.Cert, " and ", p.Key)
	return tls.LoadX509KeyPair(p.Cert, p.Key)
}

// GetCA reads the CA file from the disk or returns the rawCA if set
func (p PKI) GetCA() ([]byte, error) {
	if len(p.rawCA) > 0 {
		return p.rawCA, nil
	}

	log.Debug("loading ca from ", p.CA)
	return ioutil.ReadFile(p.CA)
}

var (
	// C stores the actual configured values
	C Config

	// Defaults are the default values for each config option, set in the Load() function
	Defaults = Config{
		Bind:            ":8080",
		BridgeInterface: "br0",
		WANInterface:    "br1",
		DiskStoragePool: "default",
		ImageDir:        "/var/lib/libvirt/images",
		ImageHost:       "http://image-host.fruit-0.entanglement.garden",
		StaticInstances: "/etc/rhyzome/static-instances",
		ImageCache:      fmt.Sprintf("%s/.cache/rhyzome/images/", os.Getenv("HOME")),
	}
)

// Load loads the configuration off the disk
func Load() {
	log.SetLevel(log.DebugLevel)
	log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp:             true,
		EnvironmentOverrideColors: true,
		CallerPrettyfier:          logPrettifier,
	})

	C = Defaults
	for _, filename := range ConfigFiles {
		jsonFile, err := os.Open(filename)
		if err != nil {
			if !os.IsNotExist(err) {
				log.Errorf("failed to open config file %s: %s", filename, err.Error())
			}
			continue
		}
		defer jsonFile.Close()

		byteValue, err := ioutil.ReadAll(jsonFile)
		if err != nil {
			log.Errorf("failed to read config file %s: %s", filename, err.Error())
			continue
		}

		err = json.Unmarshal(byteValue, &C)
		if err != nil {
			log.Errorf("error parsing config file %s: %s", filename, err.Error())
			continue
		}
		log.Info("Successfully read config from ", filename)
	}
}

func logPrettifier(f *runtime.Frame) (string, string) {
	filenameParts := strings.SplitN(f.File, "git.callpipe.com/entanglement.garden/rhyzome-libvirt/", 2)
	filename := f.File
	if len(filenameParts) > 1 {
		filename = fmt.Sprintf("%s:%d", filenameParts[1], f.Line)
	}
	s := strings.Split(f.Function, ".")
	funcname := fmt.Sprintf("%s()", s[len(s)-1])
	return funcname, filename
}
