# Rhyzome Libvirt

This is a Rhyzome component for provisioning VMs using `libvirt`.

It is expected to be used on a Linux Hypervisor with KVM support.

It facilates the creation of virtual machines, and unikernels.

## Requirements

### Runtime

* `libvirt-daemon >=5.0.0`

### Build

* `Go >=1.11`
* `libvirt-headers >=5.0.0`


## Install

There are several binaries, each can be built fairly easily:

```shell
# Build the daemon/server
go build ./cmd/rhyzome-libvirt

# Build the qemu callback hook
go build ./cmd/rhyzome-libvirt-hook-qemu

# Build the rca cli
go build ./cmd/rca
```

## Bootstrapping

*Warning: currently, there are many rough edges. eventually this will (hopefully) be easier*

Use `rca bootstrap` to begin the bootstrapping process. It will create several control VMs and eventually print out a command to do the join process.
The command will be `sudo rca join` followed by a connect string that is valid for one hour. It will also print out a URL and credentials for the
web ui. Currently it uses generated certificates and requires clicking through cert validation failures on multiple domains.

If bootstrapping fails for any reason, the `clean-bootstrap.sh` will attempt to delete all components the bootstrapping process creates.

If all goes well, the `rca join` command will update/create:
* `/etc/rhyzome/libvirt.json`
* `/etc/rhyzome/ca.pem`
* `/etc/rhyzome/step/` a `.crt` and `.key` file with a generated name in this directory

After it's finished, you should (re)start `rhyzome-libvirt`. It will connect to the grpc server created on the VM.